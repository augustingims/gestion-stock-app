package com.skysoft.app.application.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.skysoft.app.application.dto.EntrepriseDto;
import com.skysoft.app.domain.feature.company.ports.in.CompanyUseCases;
import com.skysoft.app.infrastructure.config.ApplicationProperties;
import com.skysoft.app.infrastructure.config.security.ApplicationRequestFilter;
import com.skysoft.app.infrastructure.config.security.SecurityConfiguration;
import com.skysoft.app.infrastructure.config.security.auth.ApplicationUserDetailsService;
import com.skysoft.app.infrastructure.mapper.EntrepriseMapper;
import com.skysoft.app.infrastructure.utils.JwtUtil;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(EntrepriseResource.class)
@ContextConfiguration(
    classes = {
      EntrepriseResource.class,
      JwtUtil.class,
      ApplicationProperties.class,
      SecurityConfiguration.class,
      ApplicationRequestFilter.class
    })
@AutoConfigureMockMvc
class EntrepriseResourceTest {

  @Autowired private MockMvc mockMvc;

  @MockBean private CompanyUseCases companyUseCases;

  @Autowired private ObjectMapper objectMapper;

  @MockBean private ApplicationUserDetailsService applicationUserDetailsService;
  private EntrepriseDto validEntrepriseDto;

  @BeforeEach
  void setUp() {
    validEntrepriseDto =
        EntrepriseDto.builder()
            .id(1L)
            .nom("Skysoft")
            .description("Software company")
            .codeFiscal("ABC123")
            .email("contact@skysoft.com")
            .password("password")
            .build();
  }

  @Test
  void testCreateEntrepriseWhenValidRequestThenReturnOk() throws Exception {
    when(companyUseCases.createCompany(any(), any()))
        .thenReturn(EntrepriseMapper.INSTANCE.toDomain(validEntrepriseDto));

    mockMvc
        .perform(
            post("/api/v1/entreprises")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(validEntrepriseDto)))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.nom").value(validEntrepriseDto.getNom()));
  }

  @Test
  @WithMockUser
  void testFindAllWhenNoQueryParamsThenReturnOk() throws Exception {
    when(companyUseCases.getAllCompany())
        .thenReturn(List.of(EntrepriseMapper.INSTANCE.toDomain(validEntrepriseDto)));

    mockMvc
        .perform(get("/api/v1/entreprises"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].nom").value(validEntrepriseDto.getNom()));
  }

  @Test
  @WithMockUser
  void testFindAllWhenIdQueryParamThenReturnOk() throws Exception {
    when(companyUseCases.getCompany(validEntrepriseDto.getId()))
        .thenReturn(Optional.of(EntrepriseMapper.INSTANCE.toDomain(validEntrepriseDto)));

    mockMvc
        .perform(get("/api/v1/entreprises").param("id", String.valueOf(validEntrepriseDto.getId())))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].nom").value(validEntrepriseDto.getNom()));
  }

  @Test
  @WithMockUser
  void testDeleteEntrepriseWhenValidIdThenReturnOk() throws Exception {
    mockMvc
        .perform(delete("/api/v1/entreprises/{idEntreprise}", validEntrepriseDto.getId()))
        .andExpect(status().isOk());
  }
}
