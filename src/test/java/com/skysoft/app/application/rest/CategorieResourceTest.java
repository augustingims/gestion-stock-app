package com.skysoft.app.application.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.skysoft.app.application.dto.CategorieDto;
import com.skysoft.app.domain.feature.product.ports.in.CategoryUseCases;
import com.skysoft.app.infrastructure.config.ApplicationProperties;
import com.skysoft.app.infrastructure.config.security.ApplicationRequestFilter;
import com.skysoft.app.infrastructure.config.security.SecurityConfiguration;
import com.skysoft.app.infrastructure.config.security.auth.ApplicationUserDetailsService;
import com.skysoft.app.infrastructure.mapper.CategorieMapper;
import com.skysoft.app.infrastructure.utils.JwtUtil;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(CategorieResource.class)
@ContextConfiguration(
    classes = {
      CategorieResource.class,
      JwtUtil.class,
      ApplicationProperties.class,
      SecurityConfiguration.class,
      ApplicationRequestFilter.class
    })
class CategorieResourceTest {

  @Autowired private MockMvc mockMvc;

  @MockBean private CategoryUseCases categoryUseCases;
  @MockBean private ApplicationUserDetailsService applicationUserDetailsService;

  @Autowired private ObjectMapper objectMapper;

  @Test
  @WithMockUser
  void testSaveWhenValidCategorieDtoThenReturnCategorieDto() throws Exception {
    final CategorieDto dto =
        CategorieDto.builder().code("CAT001").designation("Electronics").idEntreprise(1L).build();

    final CategorieDto saveDto =
        CategorieDto.builder()
            .id(1L)
            .code("CAT001")
            .designation("Electronics")
            .idEntreprise(1L)
            .build();

    Mockito.when(categoryUseCases.createCategory(any()))
        .thenReturn(CategorieMapper.INSTANCE.toDomain(saveDto));

    mockMvc
        .perform(
            post("/api/v1/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id").value(saveDto.getId()))
        .andExpect(jsonPath("$.code").value(saveDto.getCode()))
        .andExpect(jsonPath("$.designation").value(saveDto.getDesignation()))
        .andExpect(jsonPath("$.idEntreprise").value(saveDto.getIdEntreprise()));
  }

  @Test
  @WithMockUser
  void testFindAllWhenNoQueryParamsThenReturnAllCategories() throws Exception {
    final List<CategorieDto> categories =
        List.of(
            CategorieDto.builder()
                .id(1L)
                .code("CAT001")
                .designation("Electronics")
                .idEntreprise(1L)
                .build(),
            CategorieDto.builder()
                .id(2L)
                .code("CAT002")
                .designation("Books")
                .idEntreprise(1L)
                .build());

    Mockito.when(categoryUseCases.getAllCategory())
        .thenReturn(categories.stream().map(CategorieMapper.INSTANCE::toDomain).toList());

    mockMvc
        .perform(get("/api/v1/categories"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].id").value(categories.get(0).getId()))
        .andExpect(jsonPath("$[0].code").value(categories.get(0).getCode()))
        .andExpect(jsonPath("$[0].designation").value(categories.get(0).getDesignation()))
        .andExpect(jsonPath("$[0].idEntreprise").value(categories.get(0).getIdEntreprise()))
        .andExpect(jsonPath("$[1].id").value(categories.get(1).getId()))
        .andExpect(jsonPath("$[1].code").value(categories.get(1).getCode()))
        .andExpect(jsonPath("$[1].designation").value(categories.get(1).getDesignation()))
        .andExpect(jsonPath("$[1].idEntreprise").value(categories.get(1).getIdEntreprise()));
  }

  @Test
  @WithMockUser
  void testDeleteWhenValidCategoryIdThenReturnNoContent() throws Exception {
    final Long categoryId = 1L;
    doNothing().when(categoryUseCases).deleteCategory(categoryId);

    mockMvc
        .perform(delete("/api/v1/categories/{idCategorie}", categoryId))
        .andExpect(status().isOk());
  }
}
