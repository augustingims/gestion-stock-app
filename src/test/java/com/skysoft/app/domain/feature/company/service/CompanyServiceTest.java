package com.skysoft.app.domain.feature.company.service;

import static org.mockito.Mockito.*;

import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.feature.company.ports.out.CompanyRepository;
import com.skysoft.app.domain.feature.user.ports.out.UserRepository;
import com.skysoft.app.domain.model.Address;
import com.skysoft.app.domain.model.Company;
import com.skysoft.app.domain.model.User;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class CompanyServiceTest {
  @Mock CompanyRepository companyRepository;
  @Mock UserRepository userRepository;
  @InjectMocks CompanyService companyService;

  private Company company;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
    company =
        Company.builder()
            .nom("nom")
            .email("email")
            .codeFiscal("codeFiscal")
            .numTel("numTel")
            .siteWeb("siteWeb")
            .description("description")
            .adresse(
                Address.builder()
                    .adresse1("adresse1")
                    .pays("pays")
                    .ville("ville")
                    .codePostale("codePostale")
                    .build())
            .build();
  }

  @Test
  void given_company_without_required_data_should_throw_functional_error_exception() {
    // Given

    company = Company.builder().build();
    when(companyRepository.saveCompany(any(Company.class))).thenReturn(company);
    when(userRepository.saveUser(any(User.class))).thenReturn(User.builder().build());

    // When
    final FunctionalErrorException functionalErrorException =
        Assertions.assertThrows(
            FunctionalErrorException.class,
            () -> companyService.createCompany(company, "password"));

    // Then
    Assertions.assertEquals("L'entreprise n'est pas valide", functionalErrorException.getMessage());
  }

  @Test
  void given_company_with_required_data_should_create_company() {
    // Given
    final User user = User.builder().email("email").nom("nom").build();
    when(companyRepository.saveCompany(any(Company.class))).thenReturn(company);
    when(userRepository.saveUser(any(User.class))).thenReturn(user);

    // When
    final Company result = companyService.createCompany(company, "password");

    // Then
    verify(companyRepository, times(1)).saveCompany(any(Company.class));
    verify(userRepository, times(1)).saveUser(any(User.class));

    Assertions.assertNotNull(result);
  }

  @Test
  void testGetCompany() {
    when(companyRepository.findCompany(anyLong())).thenReturn(null);

    final Optional<Company> result = companyService.getCompany(1L);
    Assertions.assertNull(result);
  }

  @Test
  void testGetAllCompany() {
    when(companyRepository.findAllCompany()).thenReturn(Collections.emptyList());

    final List<Company> result = companyService.getAllCompany();
    Assertions.assertEquals(0, result.size());
  }
}
