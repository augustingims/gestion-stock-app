package com.skysoft.app.application.dto;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LigneVenteDto {

  private Long id;
  private VenteDto vente;
  private ArticleDto article;
  private BigDecimal quantite;
  private BigDecimal prixUnitaire;
  private Long idEntreprise;

}
