package com.skysoft.app.application.dto;


import java.time.Instant;
import java.util.List;

import com.skysoft.app.infrastructure.connector.db.entity.enumeration.EtatCommande;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CommandeClientDto {

  private Long id;
  private String code;
  private Instant dateCommande;
  private EtatCommande etatCommande;
  private ClientDto client;
  private Long idEntreprise;
  private List<LigneCommandeClientDto> ligneCommandeClients;

  public boolean isCommandeLivree() {
    return EtatCommande.LIVREE.equals(this.etatCommande);
  }
}
