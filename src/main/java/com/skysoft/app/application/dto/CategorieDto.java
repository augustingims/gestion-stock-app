package com.skysoft.app.application.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CategorieDto {

    private Long id;
    private String code;
    private String designation;
    private Long idEntreprise;

}
