package com.skysoft.app.application.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClientDto {

  private Long id;
  private String nom;
  private String prenom;
  private AdresseDto adresse;
  private String photo;
  private String mail;
  private String numTel;
  private Long idEntreprise;

  @JsonIgnore
  private List<CommandeClientDto> commandeClients;

}
