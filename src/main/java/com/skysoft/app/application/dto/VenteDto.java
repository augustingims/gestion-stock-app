package com.skysoft.app.application.dto;

import java.time.Instant;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VenteDto {
  private Long id;
  private String code;
  private Instant dateVente;
  private String commentaire;
  private List<LigneVenteDto> ligneVentes;
  private Long idEntreprise;
}
