package com.skysoft.app.application.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LigneCommandeClientDto {

  private Long id;
  private ArticleDto article;
  @JsonIgnore
  private CommandeClientDto commandeClient;
  private BigDecimal quantite;
  private BigDecimal prixUnitaire;
  private Long idEntreprise;

}
