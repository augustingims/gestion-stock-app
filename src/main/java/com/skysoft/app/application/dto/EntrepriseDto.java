package com.skysoft.app.application.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EntrepriseDto {

  private Long id;
  private String nom;
  private String description;
  private AdresseDto adresse;
  private String codeFiscal;
  private String photo;
  private String email;
  private String password;
  private String numTel;
  private String siteWeb;

  @JsonIgnore private List<UtilisateurDto> utilisateurs;
}
