package com.skysoft.app.application.dto;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ArticleDto {

  private Long id;
  private String codeArticle;
  private String designation;
  private BigDecimal prixUnitaireHt;
  private BigDecimal prixUnitaireTtc;
  private BigDecimal tauxTva;
  private String photo;
  private Long idEntreprise;
  private CategorieDto categorie;
}
