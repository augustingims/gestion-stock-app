package com.skysoft.app.application.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RolesDto {

  private Long id;
  private String roleName;

  @JsonIgnore
  private UtilisateurDto utilisateur;

}
