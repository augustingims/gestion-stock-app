package com.skysoft.app.application.dto.auth;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthenticationRequest {

    @NotBlank(message = "login not blank")
    @NotEmpty
    private String login;

    @NotEmpty(message = "Password not empty")
    private String password;
}
