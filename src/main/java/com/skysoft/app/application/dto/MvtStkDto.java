package com.skysoft.app.application.dto;

import java.math.BigDecimal;
import java.time.Instant;

import com.skysoft.app.infrastructure.connector.db.entity.enumeration.SourceMvtStk;
import com.skysoft.app.infrastructure.connector.db.entity.enumeration.TypeMvtStk;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MvtStkDto {

  private Long id;
  private Instant dateMvt;
  private BigDecimal quantite;
  private ArticleDto article;
  private TypeMvtStk typeMvtStk;
  private SourceMvtStk sourceMvtStk;
  private Long idEntreprise;
}
