package com.skysoft.app.application.dto;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LigneCommandeFournisseurDto {

  private Long id;
  private ArticleDto article;
  private CommandeFournisseurDto commandeFournisseur;
  private BigDecimal quantite;
  private BigDecimal prixUnitaire;
  private Long idEntreprise;

}
