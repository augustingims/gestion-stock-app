package com.skysoft.app.application.handler;

import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import java.util.List;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(FunctionalErrorException.class)
  public ResponseEntity<ErrorDto> handleFunctionalException(
      final FunctionalErrorException errorException, final WebRequest request) {
    final HttpStatus badRequest = HttpStatus.BAD_REQUEST;
    final ErrorDto errorDto =
        ErrorDto.builder()
            .code(errorException.getErrorCode().getCode())
            .httpCode(badRequest.value())
            .message(errorException.getMessage())
            .errors(errorException.getErrors())
            .build();
    return new ResponseEntity<>(errorDto, badRequest);
  }

  @ExceptionHandler({TechnicalErrorException.class})
  public ResponseEntity<ErrorDto> handleTechnicalException(
      final TechnicalErrorException errorException, final WebRequest request) {
    final HttpStatus badRequest = HttpStatus.BAD_REQUEST;
    final ErrorDto errorDto =
        ErrorDto.builder()
            .code(errorException.getErrorCode().getCode())
            .httpCode(badRequest.value())
            .message(errorException.getMessage())
            .build();
    return new ResponseEntity<>(errorDto, badRequest);
  }

  @ExceptionHandler({BadCredentialsException.class})
  public ResponseEntity<ErrorDto> handleBadCredentialsException(
      final BadCredentialsException errorException, final WebRequest request) {
    final HttpStatus badRequest = HttpStatus.BAD_REQUEST;
    final ErrorDto errorDto =
        ErrorDto.builder()
            .code(null)
            .httpCode(badRequest.value())
            .message(errorException.getMessage())
            .build();
    return new ResponseEntity<>(errorDto, badRequest);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      final MethodArgumentNotValidException ex,
      final HttpHeaders headers,
      final HttpStatusCode status,
      final WebRequest request) {
    final List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
    final List<String> errors =
        fieldErrors.stream()
            .map(
                fieldError ->
                    String.join(": ", fieldError.getField(), fieldError.getDefaultMessage()))
            .toList();
    final HttpStatus badRequest = HttpStatus.BAD_REQUEST;
    final ErrorDto errorDto =
        ErrorDto.builder()
            .code(ErrorCodes.METHOD_ARGUMENT_NOT_VALID.getCode())
            .httpCode(badRequest.value())
            .message(ex.getMessage())
            .errors(errors)
            .build();
    return new ResponseEntity<>(errorDto, badRequest);
  }
}
