package com.skysoft.app.application.handler;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class ErrorDto {
    private Integer httpCode;
    private Integer code;
    private String message;
    private List<String> errors = new ArrayList<>();
}
