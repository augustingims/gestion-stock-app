package com.skysoft.app.application.rest;

import com.skysoft.app.application.dto.auth.AuthenticationRequest;
import com.skysoft.app.application.dto.auth.AuthenticationResponse;
import com.skysoft.app.infrastructure.config.security.auth.ApplicationUserDetailsService;
import com.skysoft.app.infrastructure.config.security.auth.ExtendedUser;
import com.skysoft.app.infrastructure.utils.JwtUtil;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class AuthenticationResource {

  private final AuthenticationManager authenticationManager;
  private final ApplicationUserDetailsService applicationUserDetailsService;
  private final JwtUtil jwtUtil;

  @PostMapping("/authenticate")
  @SecurityRequirements(value = {})
  public ResponseEntity<AuthenticationResponse> authenticate(
      @RequestBody @Valid final AuthenticationRequest request) {
    authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(request.getLogin(), request.getPassword()));
    final UserDetails userDetails =
        applicationUserDetailsService.loadUserByUsername(request.getLogin());
    final String jwt = jwtUtil.generateToken((ExtendedUser) userDetails);
    return ResponseEntity.ok(AuthenticationResponse.builder().accessToken(jwt).build());
  }
}
