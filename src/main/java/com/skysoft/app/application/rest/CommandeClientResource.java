package com.skysoft.app.application.rest;

import com.skysoft.app.application.dto.CommandeClientDto;
import com.skysoft.app.application.dto.LigneCommandeClientDto;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.customer.ports.in.CustomerOrderUseCases;
import com.skysoft.app.domain.feature.customer.ports.in.LineCustomerOrderUseCases;
import com.skysoft.app.domain.model.enumeration.OrderStatus;
import com.skysoft.app.infrastructure.connector.db.entity.enumeration.EtatCommande;
import com.skysoft.app.infrastructure.mapper.CommandeClientMapper;
import com.skysoft.app.infrastructure.mapper.LigneCommandeClientMapper;
import java.math.BigDecimal;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class CommandeClientResource {

  private final CustomerOrderUseCases customerOrderUseCases;
  private final LineCustomerOrderUseCases lineCustomerOrderUseCases;

  @PostMapping("/commandesclients")
  public ResponseEntity<CommandeClientDto> save(@RequestBody CommandeClientDto dto)
      throws TechnicalErrorException {
    return ResponseEntity.ok(
        CommandeClientMapper.INSTANCE.toDto(
            customerOrderUseCases.createCustomerOrder(
                CommandeClientMapper.INSTANCE.toDomain(dto))));
  }

  @PatchMapping("/commandesclients/{idCommande}/etat/{etatCommande}")
  public ResponseEntity<CommandeClientDto> updateEtatCommande(
      @PathVariable("idCommande") Long idCommande,
      @PathVariable("etatCommande") EtatCommande etatCommande)
      throws TechnicalErrorException {
    return ResponseEntity.ok(
        CommandeClientMapper.INSTANCE.toDto(
            customerOrderUseCases.updateEtatCommande(
                idCommande, OrderStatus.valueOf(etatCommande.name()))));
  }

  @PatchMapping("/commandesclients/{idCommande}/line/{idLigneCommande}/quantite/{quantite}")
  public ResponseEntity<CommandeClientDto> updateQuantiteCommande(
      @PathVariable("idCommande") Long idCommande,
      @PathVariable("idLigneCommande") Long idLigneCommande,
      @PathVariable("quantite") BigDecimal quantite)
      throws TechnicalErrorException {
    return ResponseEntity.ok(
        CommandeClientMapper.INSTANCE.toDto(
            customerOrderUseCases.updateQuantiteCommande(idCommande, idLigneCommande, quantite)));
  }

  @PatchMapping("/commandesclients/{idCommande}/client/{idClient}")
  public ResponseEntity<CommandeClientDto> updateClient(
      @PathVariable("idCommande") Long idCommande, @PathVariable("idClient") Long idClient)
      throws TechnicalErrorException {
    return ResponseEntity.ok(
        CommandeClientMapper.INSTANCE.toDto(
            customerOrderUseCases.updateClient(idCommande, idClient)));
  }

  @PatchMapping("/commandesclients/{idCommande}/line/{idLigneCommande}/article/{idArticle}")
  public ResponseEntity<CommandeClientDto> updateArticle(
      @PathVariable("idCommande") Long idCommande,
      @PathVariable("idLigneCommande") Long idLigneCommande,
      @PathVariable("idArticle") Long idArticle)
      throws TechnicalErrorException {
    return ResponseEntity.ok(
        CommandeClientMapper.INSTANCE.toDto(
            customerOrderUseCases.updateArticle(idCommande, idLigneCommande, idArticle)));
  }

  @DeleteMapping("/commandesclients/{idCommande}/line/{idLigneCommande}/article")
  public ResponseEntity<CommandeClientDto> deleteArticle(
      @PathVariable("idCommande") Long idCommande,
      @PathVariable("idLigneCommande") Long idLigneCommande)
      throws TechnicalErrorException {
    return ResponseEntity.ok(
        CommandeClientMapper.INSTANCE.toDto(
            customerOrderUseCases.deleteArticle(idCommande, idLigneCommande)));
  }

  @GetMapping("/commandesclients")
  public ResponseEntity<List<CommandeClientDto>> findAll(
      @RequestParam(required = false) Long id, @RequestParam(required = false) String code) {
    return null;
  }

  @GetMapping("/commandesclients/{idCommande}/lignesCommande")
  public ResponseEntity<List<LigneCommandeClientDto>>
      findAllLignesCommandesClientByCommandeClientId(@PathVariable("idCommande") Long idCommande) {
    return ResponseEntity.ok(
        lineCustomerOrderUseCases.getAllLignesCommandesClientByCommandeClientId(idCommande).stream()
            .map(LigneCommandeClientMapper.INSTANCE::toDto)
            .toList());
  }

  @DeleteMapping("/commandesclients/{idCommandeClient}")
  public ResponseEntity<Void> delete(@PathVariable("idCommandeClient") Long id) {
    customerOrderUseCases.deleteCustomerOrder(id);
    return ResponseEntity.noContent().build();
  }
}
