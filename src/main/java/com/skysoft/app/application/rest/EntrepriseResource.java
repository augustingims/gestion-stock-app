package com.skysoft.app.application.rest;

import com.skysoft.app.application.dto.EntrepriseDto;
import com.skysoft.app.domain.feature.company.ports.in.CompanyUseCases;
import com.skysoft.app.infrastructure.mapper.EntrepriseMapper;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class EntrepriseResource {

  private final CompanyUseCases companyUseCases;

  @PostMapping("/entreprises")
  @SecurityRequirements(value = {})
  public ResponseEntity<EntrepriseDto> createEntreprise(
      @RequestBody final EntrepriseDto entrepriseDto) {
    return ResponseEntity.ok(
        EntrepriseMapper.INSTANCE.toDto(
            companyUseCases.createCompany(
                EntrepriseMapper.INSTANCE.toDomain(entrepriseDto), entrepriseDto.getPassword())));
  }

  @GetMapping("/entreprises")
  public List<EntrepriseDto> findAll(
      @RequestParam(required = false, value = "id") final Long entrepriseId) {
    if (entrepriseId != null) {
      return Collections.singletonList(
          companyUseCases
              .getCompany(entrepriseId)
              .map(EntrepriseMapper.INSTANCE::toDto)
              .orElseThrow());
    } else {
      return companyUseCases.getAllCompany().stream()
          .map(EntrepriseMapper.INSTANCE::toDto)
          .toList();
    }
  }

  @DeleteMapping("/entreprises/{idEntreprise}")
  public void deleteEntreprise(@PathVariable("idEntreprise") final Long id) {}
}
