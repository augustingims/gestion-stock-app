package com.skysoft.app.application.rest;

import com.skysoft.app.application.dto.ChangerMotDePasseDto;
import com.skysoft.app.application.dto.UtilisateurDto;
import com.skysoft.app.domain.feature.user.ports.in.UserUseCases;
import com.skysoft.app.infrastructure.mapper.UtilisateurMapper;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class UtilisateurResource {

  private final UserUseCases userUseCases;

  @PostMapping("/utilisateurs")
  public UtilisateurDto save(@RequestBody final UtilisateurDto dto) {
    return UtilisateurMapper.INSTANCE.toDto(
        userUseCases.createUser(UtilisateurMapper.INSTANCE.toDomain(dto)));
  }

  @GetMapping("/utilisateurs/account")
  public UtilisateurDto currentUser() {
    return UtilisateurMapper.INSTANCE.toDto(userUseCases.getCurrentUser().orElseThrow());
  }

  @PutMapping("/utilisateurs/{idUtilisateur}/password")
  public UtilisateurDto changePassword(
      @PathVariable("idUtilisateur") final Long id, @RequestBody final ChangerMotDePasseDto dto) {
    return UtilisateurMapper.INSTANCE.toDto(
        userUseCases.changePassword(id, dto.getMotDePasse(), dto.getConfirmMotDePasse()));
  }

  @GetMapping("/utilisateurs")
  @Secured({"ADMIN"})
  public List<UtilisateurDto> findAll(
      @RequestParam(required = false) final Long id,
      @RequestParam(required = false) final String email) {
    return userUseCases.getAllUsers(id, email).stream()
        .map(UtilisateurMapper.INSTANCE::toDto)
        .toList();
  }

  @DeleteMapping("/utilisateurs/{idUtilisateur}")
  public void delete(@PathVariable("idUtilisateur") final Long id) {}
}
