package com.skysoft.app.application.rest;

import com.skysoft.app.application.dto.FournisseurDto;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.supplier.ports.in.SupplierUseCases;
import com.skysoft.app.infrastructure.mapper.FournisseurMapper;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class FournisseurResource {

  private final SupplierUseCases supplierUseCases;

  @PostMapping("/fournisseurs")
  public FournisseurDto save(@RequestBody FournisseurDto dto) {
    return FournisseurMapper.INSTANCE.toDto(
        supplierUseCases.createSupplier(FournisseurMapper.INSTANCE.toDomain(dto)));
  }

  @GetMapping("/fournisseurs")
  public List<FournisseurDto> findAll(@RequestParam(required = false) Long id)
      throws TechnicalErrorException {
    if (id != null) {
      return Collections.singletonList(
          supplierUseCases
              .getSupplierById(id)
              .map(FournisseurMapper.INSTANCE::toDto)
              .orElseThrow());
    } else {
      return supplierUseCases.getAllSupplier().stream()
          .map(FournisseurMapper.INSTANCE::toDto)
          .toList();
    }
  }

  @DeleteMapping("/fournisseurs/{idFournisseur}")
  public void delete(@PathVariable("idFournisseur") Long id) {
    supplierUseCases.deleteSupplier(id);
  }
}
