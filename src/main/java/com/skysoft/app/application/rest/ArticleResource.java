package com.skysoft.app.application.rest;

import com.skysoft.app.application.dto.ArticleDto;
import com.skysoft.app.application.dto.LigneCommandeClientDto;
import com.skysoft.app.application.dto.LigneCommandeFournisseurDto;
import com.skysoft.app.application.dto.LigneVenteDto;
import com.skysoft.app.domain.feature.customer.ports.in.LineCustomerOrderUseCases;
import com.skysoft.app.domain.feature.product.ports.in.LineSalesUseCases;
import com.skysoft.app.domain.feature.product.ports.in.ProductUseCases;
import com.skysoft.app.domain.feature.supplier.ports.in.LineSupplierOrderUseCases;
import com.skysoft.app.infrastructure.mapper.*;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class ArticleResource {

  private final ProductUseCases productUseCases;
  private final LineSalesUseCases lineSalesUseCases;
  private final LineCustomerOrderUseCases lineCustomerOrderUseCases;
  private final LineSupplierOrderUseCases lineSupplierOrderUseCases;

  @PostMapping(
      value = "/articles",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ArticleDto save(@RequestBody final ArticleDto articleDto) {
    return ArticleMapper.INSTANCE.toDto(
        productUseCases.createArticle(ArticleMapper.INSTANCE.toDomain(articleDto)));
  }

  @GetMapping(value = "/articles", produces = MediaType.APPLICATION_JSON_VALUE)
  public List<ArticleDto> findAll(
      @RequestParam(required = false) final Long id,
      @RequestParam(required = false) final String code,
      @RequestParam(required = false) final Long category) {
    return productUseCases.getAllProducts(id, code).stream()
        .map(ArticleMapper.INSTANCE::toDto)
        .toList();
  }

  @GetMapping(
      value = "/articles/{idArticle}/historiquevente",
      produces = MediaType.APPLICATION_JSON_VALUE)
  public List<LigneVenteDto> findHistoriqueVentes(@PathVariable("idArticle") final Long idArticle) {
    return lineSalesUseCases.findHistoriqueVentes(idArticle).stream()
        .map(LigneVenteMapper.INSTANCE::toDto)
        .collect(Collectors.toList());
  }

  @GetMapping(
      value = "/articles/{idArticle}/historiquecommandeclient",
      produces = MediaType.APPLICATION_JSON_VALUE)
  public List<LigneCommandeClientDto> findHistoriaueCommandeClient(
      @PathVariable("idArticle") final Long idArticle) {
    return lineCustomerOrderUseCases.getHistoriaueCommandeClient(idArticle).stream()
        .map(LigneCommandeClientMapper.INSTANCE::toDto)
        .toList();
  }

  @GetMapping(
      value = "/articles/{idArticle}/historiquecommandefournisseur",
      produces = MediaType.APPLICATION_JSON_VALUE)
  public List<LigneCommandeFournisseurDto> findHistoriqueCommandeFournisseur(
      @PathVariable("idArticle") final Long idArticle) {
    return lineSupplierOrderUseCases.getHistoriqueCommandeFournisseur(idArticle).stream()
        .map(LigneCommandeFournisseurMapper.INSTANCE::toDto)
        .toList();
  }

  @DeleteMapping(value = "/articles/{idArticle}")
  public void delete(@PathVariable("idArticle") final Long id) {
    productUseCases.deleteArticle(id);
  }
}
