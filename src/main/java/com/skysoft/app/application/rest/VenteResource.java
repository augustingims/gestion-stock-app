package com.skysoft.app.application.rest;

import com.skysoft.app.application.dto.VenteDto;
import com.skysoft.app.domain.feature.product.ports.in.SalesUseCases;
import com.skysoft.app.infrastructure.mapper.VenteMapper;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class VenteResource {

  private final SalesUseCases salesUseCases;

  @PostMapping("/ventes")
  public VenteDto save(@RequestBody VenteDto venteDto) {
    return VenteMapper.INSTANCE.toDto(
        salesUseCases.createSales(VenteMapper.INSTANCE.toDomain(venteDto)));
  }

  @GetMapping("/ventes")
  public List<VenteDto> findAll(
      @RequestParam(required = false) Long id, @RequestParam(required = false) String code) {
    return null;
  }

  @DeleteMapping("/ventes/{idVente}")
  public void delete(@PathVariable("idVente") Long id) {
    salesUseCases.deleteSales(id);
  }
}
