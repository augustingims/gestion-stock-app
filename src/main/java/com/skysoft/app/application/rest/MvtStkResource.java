package com.skysoft.app.application.rest;

import com.skysoft.app.application.dto.MvtStkDto;
import com.skysoft.app.domain.feature.product.ports.in.InventoryMovementUseCases;
import com.skysoft.app.infrastructure.mapper.MvtStkMapper;
import java.math.BigDecimal;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class MvtStkResource {

  private final InventoryMovementUseCases inventoryMovementUseCases;

  @GetMapping("/mvtstk/{idArticle}/stockreel")
  public BigDecimal stockReelArticle(@PathVariable("idArticle") Long idArticle) {
    return inventoryMovementUseCases.stockReelArticle(idArticle);
  }

  @GetMapping("/mvtstk/{idArticle}/article")
  public List<MvtStkDto> mvtStkArticle(@PathVariable("idArticle") Long idArticle) {
    return inventoryMovementUseCases.mvtStkArticle(idArticle).stream()
        .map(MvtStkMapper.INSTANCE::toDto)
        .toList();
  }

  @PostMapping("/mvtstk/entree")
  public MvtStkDto entreeStock(@RequestBody MvtStkDto dto) {
    return MvtStkMapper.INSTANCE.toDto(
        inventoryMovementUseCases.entreeStock(MvtStkMapper.INSTANCE.toDomain(dto)));
  }

  @PostMapping("/mvtstk/sortie")
  public MvtStkDto sortieStock(@RequestBody MvtStkDto dto) {
    return MvtStkMapper.INSTANCE.toDto(
        inventoryMovementUseCases.sortieStock(MvtStkMapper.INSTANCE.toDomain(dto)));
  }

  @PostMapping("/mvtstk/correctionpos")
  public MvtStkDto correctionStockPos(@RequestBody MvtStkDto dto) {
    return MvtStkMapper.INSTANCE.toDto(
        inventoryMovementUseCases.correctionStockPos(MvtStkMapper.INSTANCE.toDomain(dto)));
  }

  @PostMapping("/mvtstk/correctionneg")
  public MvtStkDto correctionStockNeg(@RequestBody MvtStkDto dto) {
    return MvtStkMapper.INSTANCE.toDto(
        inventoryMovementUseCases.correctionStockNeg(MvtStkMapper.INSTANCE.toDomain(dto)));
  }
}
