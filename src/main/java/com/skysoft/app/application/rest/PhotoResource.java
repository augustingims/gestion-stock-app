package com.skysoft.app.application.rest;

import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.service.in.PictureUseCase;
import java.io.IOException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class PhotoResource {

  private final PictureUseCase pictureUseCase;

  @PostMapping("/photos/{id}/item/{title}/name/{context}")
  public Object savePhoto(
      @PathVariable("context") final String context,
      @PathVariable("id") final Long id,
      @RequestPart("file") final MultipartFile photo,
      @PathVariable("title") final String title)
      throws IOException, TechnicalErrorException {
    return pictureUseCase.savePicture(context, id, photo.getInputStream(), title);
  }
}
