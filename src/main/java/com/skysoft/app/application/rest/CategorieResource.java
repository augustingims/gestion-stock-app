package com.skysoft.app.application.rest;

import com.skysoft.app.application.dto.CategorieDto;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.product.ports.in.CategoryUseCases;
import com.skysoft.app.infrastructure.mapper.CategorieMapper;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class CategorieResource {

  private final CategoryUseCases categoryUseCases;

  @PostMapping(
      value = "/categories",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public CategorieDto save(@RequestBody final CategorieDto dto) {
    return CategorieMapper.INSTANCE.toDto(
        categoryUseCases.createCategory(CategorieMapper.INSTANCE.toDomain(dto)));
  }

  @GetMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_VALUE)
  // @Cacheable(cacheNames = "findCategoryById")
  public List<CategorieDto> findAll(
      @RequestParam(required = false) final Long id,
      @RequestParam(required = false) final String code)
      throws TechnicalErrorException {
    if (id != null) {
      return Collections.singletonList(
          categoryUseCases.getCategoryById(id).map(CategorieMapper.INSTANCE::toDto).orElse(null));
    } else {
      return categoryUseCases.getAllCategory().stream()
          .map(CategorieMapper.INSTANCE::toDto)
          .toList();
    }
  }

  @DeleteMapping(value = "/categories/{idCategorie}")
  public void delete(@PathVariable("idCategorie") final Long id) {
    categoryUseCases.deleteCategory(id);
  }
}
