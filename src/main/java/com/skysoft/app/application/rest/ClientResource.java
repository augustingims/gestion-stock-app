package com.skysoft.app.application.rest;

import com.skysoft.app.application.dto.ClientDto;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.customer.ports.in.CustomerUseCases;
import com.skysoft.app.infrastructure.mapper.ClientMapper;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class ClientResource {

  private final CustomerUseCases customerUseCases;

  @PostMapping(
      value = "/clients",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ClientDto save(@RequestBody ClientDto dto) {
    return ClientMapper.INSTANCE.toDto(
        customerUseCases.createCustomer(ClientMapper.INSTANCE.toDomain(dto)));
  }

  @GetMapping(value = "/clients", produces = MediaType.APPLICATION_JSON_VALUE)
  public List<ClientDto> findAll(@RequestParam(required = false) Long id)
      throws TechnicalErrorException {
    if (id != null) {
      return Collections.singletonList(
          customerUseCases.getCustomerById(id).map(ClientMapper.INSTANCE::toDto).orElseThrow());
    } else {
      return customerUseCases.getAllCustomer().stream().map(ClientMapper.INSTANCE::toDto).toList();
    }
  }

  @DeleteMapping(value = "/clients/{idClient}")
  public void delete(@PathVariable("idClient") Long id) {
    customerUseCases.deleteCustomer(id);
  }
}
