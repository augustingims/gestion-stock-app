package com.skysoft.app.application.rest;

import com.skysoft.app.application.dto.CommandeFournisseurDto;
import com.skysoft.app.application.dto.LigneCommandeFournisseurDto;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.supplier.ports.in.LineSupplierOrderUseCases;
import com.skysoft.app.domain.feature.supplier.ports.in.SupplierOrderUseCases;
import com.skysoft.app.domain.model.enumeration.OrderStatus;
import com.skysoft.app.infrastructure.connector.db.entity.enumeration.EtatCommande;
import com.skysoft.app.infrastructure.mapper.CommandeFournisseurMapper;
import com.skysoft.app.infrastructure.mapper.LigneCommandeFournisseurMapper;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class CommandeFournisseurResource {

  private final SupplierOrderUseCases supplierOrderUseCases;
  private final LineSupplierOrderUseCases lineSupplierOrderUseCases;

  @PostMapping("/commandesfournisseurs")
  public CommandeFournisseurDto save(@RequestBody CommandeFournisseurDto dto)
      throws TechnicalErrorException {
    return CommandeFournisseurMapper.INSTANCE.toDto(
        supplierOrderUseCases.createSupplierOrder(
            CommandeFournisseurMapper.INSTANCE.toDomain(dto)));
  }

  @PatchMapping("/commandesfournisseurs/{idCommande}/etat/{etatCommande}")
  public CommandeFournisseurDto updateEtatCommande(
      @PathVariable("idCommande") Long idCommande,
      @PathVariable("etatCommande") EtatCommande etatCommande)
      throws TechnicalErrorException {
    return CommandeFournisseurMapper.INSTANCE.toDto(
        supplierOrderUseCases.updateEtatCommande(
            idCommande, OrderStatus.valueOf(etatCommande.name())));
  }

  @PatchMapping("/commandesfournisseurs/{idCommande}/line/{idLigneCommande}/quantite/{quantite}")
  public CommandeFournisseurDto updateQuantiteCommande(
      @PathVariable("idCommande") Long idCommande,
      @PathVariable("idLigneCommande") Long idLigneCommande,
      @PathVariable("quantite") BigDecimal quantite)
      throws TechnicalErrorException {
    return CommandeFournisseurMapper.INSTANCE.toDto(
        supplierOrderUseCases.updateQuantiteCommande(idCommande, idLigneCommande, quantite));
  }

  @PatchMapping("/commandesfournisseurs/{idCommande}/fournisseur/{idFournisseur}")
  public CommandeFournisseurDto updateFournisseur(
      @PathVariable("idCommande") Long idCommande,
      @PathVariable("idFournisseur") Long idFournisseur)
      throws TechnicalErrorException {
    return CommandeFournisseurMapper.INSTANCE.toDto(
        supplierOrderUseCases.updateFournisseur(idCommande, idFournisseur));
  }

  @PatchMapping("/commandesfournisseurs/{idCommande}/line/{idLigneCommande}/article/{idArticle}")
  public CommandeFournisseurDto updateArticle(
      @PathVariable("idCommande") Long idCommande,
      @PathVariable("idLigneCommande") Long idLigneCommande,
      @PathVariable("idArticle") Long idArticle)
      throws TechnicalErrorException {
    return CommandeFournisseurMapper.INSTANCE.toDto(
        supplierOrderUseCases.updateArticleInSupplierOrder(idCommande, idLigneCommande, idArticle));
  }

  @DeleteMapping("/commandesfournisseurs/{idCommande}/line/{idLigneCommande}/article")
  public CommandeFournisseurDto deleteArticle(
      @PathVariable("idCommande") Long idCommande,
      @PathVariable("idLigneCommande") Long idLigneCommande)
      throws TechnicalErrorException {
    return CommandeFournisseurMapper.INSTANCE.toDto(
        supplierOrderUseCases.deleteArticleInSupplierOrder(idCommande, idLigneCommande));
  }

  @GetMapping("/commandesfournisseurs/{idCommandeFournisseur}")
  public CommandeFournisseurDto findById(@PathVariable("idCommandeFournisseur") Long id)
      throws TechnicalErrorException {
    return supplierOrderUseCases
        .getSupplierOrderById(id)
        .map(CommandeFournisseurMapper.INSTANCE::toDto)
        .orElseThrow();
  }

  @GetMapping("/commandesfournisseurs")
  public List<CommandeFournisseurDto> findAll(@RequestParam(required = false) String code)
      throws TechnicalErrorException {
    if (code != null) {
      return Collections.singletonList(
          supplierOrderUseCases
              .getSupplierOrderByCode(code)
              .map(CommandeFournisseurMapper.INSTANCE::toDto)
              .orElseThrow());
    } else {
      return supplierOrderUseCases.getAllSupplierOrder().stream()
          .map(CommandeFournisseurMapper.INSTANCE::toDto)
          .toList();
    }
  }

  @GetMapping("/commandesfournisseurs/{idCommande}/lignesCommande")
  public List<LigneCommandeFournisseurDto> findAllLignesCommandesFournisseurByCommandeFournisseurId(
      @PathVariable("idCommande") Long idCommande) {
    return lineSupplierOrderUseCases
        .getAllLignesCommandesFournisseurByCommandeFournisseurId(idCommande)
        .stream()
        .map(LigneCommandeFournisseurMapper.INSTANCE::toDto)
        .toList();
  }

  @DeleteMapping("/commandesfournisseurs/{idCommandeFournisseur}")
  public void deleteSupplierOrder(@PathVariable("idCommandeFournisseur") Long id) {
    supplierOrderUseCases.deleteSupplierOrder(id);
  }
}
