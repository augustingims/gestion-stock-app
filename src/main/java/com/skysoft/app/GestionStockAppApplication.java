package com.skysoft.app;

import com.skysoft.app.infrastructure.config.ApplicationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
@EnableConfigurationProperties({ApplicationProperties.class})
public class GestionStockAppApplication {

  public static void main(final String[] args) {
    SpringApplication.run(GestionStockAppApplication.class, args);
  }
}
