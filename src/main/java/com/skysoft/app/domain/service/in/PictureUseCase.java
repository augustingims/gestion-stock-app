package com.skysoft.app.domain.service.in;

import com.skysoft.app.domain.exception.TechnicalErrorException;
import java.io.InputStream;

public interface PictureUseCase {
  Object savePicture(String context, Long id, InputStream photo, String titre)
      throws TechnicalErrorException;
}
