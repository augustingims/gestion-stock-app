package com.skysoft.app.domain.service;

import com.skysoft.app.domain.exception.TechnicalErrorException;
import java.io.InputStream;
import java.util.Optional;

public interface PictureStrategy<T> {
  Optional<T> upload(Long id, InputStream photo, String titre) throws TechnicalErrorException;
}
