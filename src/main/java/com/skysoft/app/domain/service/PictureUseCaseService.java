package com.skysoft.app.domain.service;

import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.company.service.CompanyPictureStrategyService;
import com.skysoft.app.domain.feature.customer.service.CustomerPictureStrategyService;
import com.skysoft.app.domain.feature.product.service.ProductPictureStrategyService;
import com.skysoft.app.domain.feature.supplier.service.SupplierPictureStrategyService;
import com.skysoft.app.domain.feature.user.service.UserPictureStrategyService;
import com.skysoft.app.domain.service.in.PictureUseCase;
import java.io.InputStream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class PictureUseCaseService implements PictureUseCase {
  private final BeanFactory beanFactory;
  private PictureStrategy pictureStrategy;

  @Override
  public Object savePicture(
      final String context, final Long id, final InputStream photo, final String titre)
      throws TechnicalErrorException {
    pictureContext(context);
    return pictureStrategy.upload(id, photo, titre);
  }

  private void pictureContext(final String context) throws TechnicalErrorException {
    switch (context) {
      case "article":
        pictureStrategy =
            beanFactory.getBean(
                "productPictureStrategyService", ProductPictureStrategyService.class);
        break;
      case "client":
        pictureStrategy =
            beanFactory.getBean(
                "customerPictureStrategyService", CustomerPictureStrategyService.class);
        break;
      case "fournisseur":
        pictureStrategy =
            beanFactory.getBean(
                "supplierPictureStrategyService", SupplierPictureStrategyService.class);
        break;
      case "entreprise":
        pictureStrategy =
            beanFactory.getBean(
                "companyPictureStrategyService", CompanyPictureStrategyService.class);
        break;
      case "utilisateur":
        pictureStrategy =
            beanFactory.getBean("userPictureStrategyService", UserPictureStrategyService.class);
        break;
      default:
        throw new TechnicalErrorException(
            "Context inconnu pour l'enregistrement de la photo", ErrorCodes.UNKNOWN_CONTEXT);
    }
  }
}
