package com.skysoft.app.domain.validator;

import com.skysoft.app.domain.model.SupplierOrder;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;

public class SupplierOrderValidator {

  public static List<String> validate(SupplierOrder supplierOrder) {
    List<String> errors = new ArrayList<>();
    if (supplierOrder == null) {
      errors.add("Veuillez renseigner le code de la commande");
      errors.add("Veuillez renseigner la date de la commande");
      errors.add("Veuillez renseigner l'etat de la commande");
      errors.add("Veuillez renseigner le client");
      return errors;
    }

    if (!StringUtils.hasLength(supplierOrder.getCode())) {
      errors.add("Veuillez renseigner le code de la commande");
    }
    if (supplierOrder.getDateCommande() == null) {
      errors.add("Veuillez renseigner la date de la commande");
    }
    if (!StringUtils.hasLength(supplierOrder.getEtatCommande().toString())) {
      errors.add("Veuillez renseigner l'etat de la commande");
    }
    if (supplierOrder.getFournisseur() == null || supplierOrder.getFournisseur().getId() == null) {
      errors.add("Veuillez renseigner le fournisseur");
    }

    return errors;
  }
}
