package com.skysoft.app.domain.validator;

import com.skysoft.app.domain.model.Company;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;

public class CompanyValidator {

  public static List<String> validate(Company company) {
    List<String> errors = new ArrayList<>();
    if (company == null) {
      errors.add("Veuillez renseigner le nom de l'entreprise");
      errors.add("Veuillez renseigner la description de l'entreprise");
      errors.add("Veuillez renseigner le code fiscal de l'entreprise");
      errors.add("Veuillez renseigner l'email de l'entreprise");
      errors.add("Veuillez renseigner le numero de telephone de l'entreprise");
      errors.addAll(AddressValidator.validate(null));
      return errors;
    }

    if (!StringUtils.hasLength(company.getNom())) {
      errors.add("Veuillez renseigner le nom de l'entreprise");
    }
    if (!StringUtils.hasLength(company.getDescription())) {
      errors.add("Veuillez renseigner la description de l'entreprise");
    }
    if (!StringUtils.hasLength(company.getCodeFiscal())) {
      errors.add("Veuillez renseigner le code fiscal de l'entreprise");
    }
    if (!StringUtils.hasLength(company.getEmail())) {
      errors.add("Veuillez renseigner l'email de l'entreprise");
    }
    if (!StringUtils.hasLength(company.getNumTel())) {
      errors.add("Veuillez renseigner le numero de telephone de l'entreprise");
    }

    errors.addAll(AddressValidator.validate(company.getAdresse()));
    return errors;
  }
}
