package com.skysoft.app.domain.validator;

import com.skysoft.app.domain.model.Address;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;

public class AddressValidator {

  private AddressValidator() {
    throw new IllegalStateException("Adresse validator");
  }

  public static List<String> validate(Address address) {
    List<String> errors = new ArrayList<>();
    if (address == null) {
      errors.add("Veuillez renseigner l'adresse 1");
      errors.add("Veuillez renseigner la ville");
      errors.add("Veuillez renseigner le pays");
      errors.add("Veuillez renseigner le code postal");
      return errors;
    }
    if (!StringUtils.hasText(address.getAdresse1())) {
      errors.add("Veuillez renseigner l'adresse 1");
    }
    if (!StringUtils.hasText(address.getVille())) {
      errors.add("Veuillez renseigner la ville");
    }
    if (!StringUtils.hasText(address.getPays())) {
      errors.add("Veuillez renseigner le pays");
    }
    if (!StringUtils.hasText(address.getCodePostale())) {
      errors.add("Veuillez renseigner le code postal");
    }
    return errors;
  }
}
