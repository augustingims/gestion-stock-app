package com.skysoft.app.domain.validator;

import com.skysoft.app.domain.model.CustomerOrder;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;

public class CustomerOrderValidator {

  public static List<String> validate(CustomerOrder customerOrder) {
    List<String> errors = new ArrayList<>();
    if (customerOrder == null) {
      errors.add("Veuillez renseigner le code de la commande");
      errors.add("Veuillez renseigner la date de la commande");
      errors.add("Veuillez renseigner l'etat de la commande");
      errors.add("Veuillez renseigner le client");
      return errors;
    }

    if (!StringUtils.hasLength(customerOrder.getCode())) {
      errors.add("Veuillez renseigner le code de la commande");
    }
    if (customerOrder.getDateCommande() == null) {
      errors.add("Veuillez renseigner la date de la commande");
    }
    if (!StringUtils.hasLength(customerOrder.getEtatCommande().toString())) {
      errors.add("Veuillez renseigner l'etat de la commande");
    }
    if (customerOrder.getClient() == null || customerOrder.getClient().getId() == null) {
      errors.add("Veuillez renseigner le client");
    }

    return errors;
  }
}
