package com.skysoft.app.domain.validator;

import com.skysoft.app.domain.model.Category;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;

public class CategoryValidator {
  public static List<String> validate(Category category) {
    List<String> errors = new ArrayList<>();
    if (null == category) {
      errors.add("Veuillez renseigner le code de la categorie");
      errors.add("Veuillez renseigner la designation");
      return errors;
    }
    if (!StringUtils.hasText(category.getCode())) {
      errors.add("Veuillez renseigner le code de la categorie");
    }
    if (!StringUtils.hasText(category.getDesignation())) {
      errors.add("Veuillez renseigner la designation");
    }
    return errors;
  }
}
