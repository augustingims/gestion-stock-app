package com.skysoft.app.domain.validator;

import com.skysoft.app.domain.model.Sales;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;

public class SalesValidator {

  public static List<String> validate(Sales sales) {
    List<String> errors = new ArrayList<>();
    if (sales == null) {
      errors.add("Veuillez renseigner le code de la commande");
      errors.add("Veuillez renseigner la date de la commande");
      return errors;
    }

    if (!StringUtils.hasLength(sales.getCode())) {
      errors.add("Veuillez renseigner le code de la commande");
    }
    if (sales.getDateVente() == null) {
      errors.add("Veuillez renseigner la date de la commande");
    }

    return errors;
  }
}
