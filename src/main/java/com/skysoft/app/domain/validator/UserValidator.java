package com.skysoft.app.domain.validator;

import com.skysoft.app.domain.model.User;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;

public class UserValidator {

  public static List<String> validate(User user) {
    List<String> errors = new ArrayList<>();

    if (user == null) {
      errors.add("Veuillez renseigner le nom d'utilisateur");
      errors.add("Veuillez renseigner le prenom d'utilisateur");
      errors.add("Veuillez renseigner le mot de passe d'utilisateur");
      errors.add("Veuillez renseigner l'adresse d'utilisateur");
      errors.addAll(AddressValidator.validate(null));
      return errors;
    }

    if (!StringUtils.hasLength(user.getNom())) {
      errors.add("Veuillez renseigner le nom d'utilisateur");
    }
    if (!StringUtils.hasLength(user.getPrenom())) {
      errors.add("Veuillez renseigner le prenom d'utilisateur");
    }
    if (!StringUtils.hasLength(user.getEmail())) {
      errors.add("Veuillez renseigner l'email d'utilisateur");
    }
    if (!StringUtils.hasLength(user.getMotDePasse())) {
      errors.add("Veuillez renseigner le mot de passe d'utilisateur");
    }
    if (user.getDateNaissance() == null) {
      errors.add("Veuillez renseigner la date de naissance d'utilisateur");
    }
    errors.addAll(AddressValidator.validate(user.getAdresse()));

    return errors;
  }
}
