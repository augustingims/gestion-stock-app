package com.skysoft.app.domain.validator;

import com.skysoft.app.domain.model.Supplier;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;

public class SupplierValidator {

  public static List<String> validate(Supplier supplier) {
    List<String> errors = new ArrayList<>();

    if (supplier == null) {
      errors.add("Veuillez renseigner le nom du fournisseur");
      errors.add("Veuillez renseigner le prenom du fournisseur");
      errors.add("Veuillez renseigner le Mail du fournisseur");
      errors.add("Veuillez renseigner le numero de telephone du fournisseur");
      errors.addAll(AddressValidator.validate(null));
      return errors;
    }

    if (!StringUtils.hasLength(supplier.getNom())) {
      errors.add("Veuillez renseigner le nom du fournisseur");
    }
    if (!StringUtils.hasLength(supplier.getPrenom())) {
      errors.add("Veuillez renseigner le prenom du fournisseur");
    }
    if (!StringUtils.hasLength(supplier.getMail())) {
      errors.add("Veuillez renseigner le Mail du fournisseur");
    }
    if (!StringUtils.hasLength(supplier.getNumTel())) {
      errors.add("Veuillez renseigner le numero de telephone du fournisseur");
    }
    errors.addAll(AddressValidator.validate(supplier.getAdresse()));
    return errors;
  }
}
