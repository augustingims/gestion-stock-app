package com.skysoft.app.domain.validator;

import com.skysoft.app.domain.model.Product;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;

public class ProductValidator {
  public static List<String> validate(Product product) {
    List<String> errors = new ArrayList<>();
    if (product == null) {
      errors.add("Veuillez renseigner le code de l'article");
      errors.add("Veuillez renseigner la designation de l'article");
      errors.add("Veuillez renseigner le prix unitaire HT de l'article");
      errors.add("Veuillez renseigner le taux tva de l'article");
      errors.add("Veuillez renseigner le prix unitaire TTC de l'article");
      errors.add("Veuillez selectionner la categorie");

      return errors;
    }
    if (!StringUtils.hasText(product.getCodeArticle())) {
      errors.add("Veuillez renseigner le code de l'article");
    }
    if (!StringUtils.hasText(product.getDesignation())) {
      errors.add("Veuillez renseigner la designation de l'article");
    }
    if (null == product.getPrixUnitaireHt()) {
      errors.add("Veuillez renseigner le prix unitaire HT de l'article");
    }
    if (null == product.getTauxTva()) {
      errors.add("Veuillez renseigner le taux tva de l'article");
    }
    if (null == product.getPrixUnitaireTtc()) {
      errors.add("Veuillez renseigner le prix unitaire TTC de l'article");
    }
    if (null == product.getCategorie()) {
      errors.add("Veuillez selectionner la categorie");
    }
    return errors;
  }
}
