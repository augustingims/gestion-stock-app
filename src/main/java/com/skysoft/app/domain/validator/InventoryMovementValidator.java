package com.skysoft.app.domain.validator;

import com.skysoft.app.domain.model.InventoryMovement;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;

public class InventoryMovementValidator {

  public static List<String> validate(InventoryMovement inventoryMovement) {
    List<String> errors = new ArrayList<>();
    if (inventoryMovement == null) {
      errors.add("Veuillez renseigner la date du mouvenent");
      errors.add("Veuillez renseigner la quantite du mouvenent");
      errors.add("Veuillez renseigner l'article");
      errors.add("Veuillez renseigner le type du mouvement");

      return errors;
    }
    if (inventoryMovement.getDateMvt() == null) {
      errors.add("Veuillez renseigner la date du mouvenent");
    }
    if (inventoryMovement.getQuantite() == null
        || inventoryMovement.getQuantite().compareTo(BigDecimal.ZERO) == 0) {
      errors.add("Veuillez renseigner la quantite du mouvenent");
    }
    if (inventoryMovement.getArticle() == null || inventoryMovement.getArticle().getId() == null) {
      errors.add("Veuillez renseigner l'article");
    }
    if (!StringUtils.hasLength(inventoryMovement.getTypeMvtStk().name())) {
      errors.add("Veuillez renseigner le type du mouvement");
    }

    return errors;
  }
}
