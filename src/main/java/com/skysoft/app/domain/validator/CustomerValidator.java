package com.skysoft.app.domain.validator;

import com.skysoft.app.domain.model.Customer;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;

public class CustomerValidator {

  public static List<String> validate(Customer customer) {
    List<String> errors = new ArrayList<>();

    if (customer == null) {
      errors.add("Veuillez renseigner le nom du client");
      errors.add("Veuillez renseigner le prenom du client");
      errors.add("Veuillez renseigner le Mail du client");
      errors.add("Veuillez renseigner le numero de telephone du client");
      errors.addAll(AddressValidator.validate(null));
      return errors;
    }

    if (!StringUtils.hasLength(customer.getNom())) {
      errors.add("Veuillez renseigner le nom du client");
    }
    if (!StringUtils.hasLength(customer.getPrenom())) {
      errors.add("Veuillez renseigner le prenom du client");
    }
    if (!StringUtils.hasLength(customer.getMail())) {
      errors.add("Veuillez renseigner le Mail du client");
    }
    if (!StringUtils.hasLength(customer.getNumTel())) {
      errors.add("Veuillez renseigner le numero de telephone du client");
    }
    errors.addAll(AddressValidator.validate(customer.getAdresse()));
    return errors;
  }
}
