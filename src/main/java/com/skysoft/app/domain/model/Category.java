package com.skysoft.app.domain.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Category {
    private Long id;
    private String code;
    private String designation;
    private Long idEntreprise;
}
