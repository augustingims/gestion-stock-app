package com.skysoft.app.domain.model;

import java.time.Instant;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User {
  private Long id;
  private String nom;
  private String prenom;
  private String email;
  private Instant dateNaissance;
  private String motDePasse;
  private Address adresse;
  private String photo;
  private Company entreprise;
  private List<Authority> roles;
}
