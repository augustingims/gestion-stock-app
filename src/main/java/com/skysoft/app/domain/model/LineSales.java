package com.skysoft.app.domain.model;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LineSales {
    private Long id;
    private Sales vente;
    private Product article;
    private BigDecimal quantite;
    private BigDecimal prixUnitaire;
    private Long idEntreprise;
}
