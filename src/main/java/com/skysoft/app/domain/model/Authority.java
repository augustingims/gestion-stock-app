package com.skysoft.app.domain.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Authority {
  private Long id;
  private String roleName;
  private User utilisateur;
}
