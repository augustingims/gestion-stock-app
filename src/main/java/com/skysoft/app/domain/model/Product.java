package com.skysoft.app.domain.model;

import java.math.BigDecimal;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Product {
  private Long id;
  private String codeArticle;
  private String designation;
  private BigDecimal prixUnitaireHt;
  private BigDecimal prixUnitaireTtc;
  private BigDecimal tauxTva;
  private String photo;
  private Long idEntreprise;
  private Category categorie;

  private List<LineSales> ligneVentes;
  private List<LineSupplierOrder> ligneCommandeFournisseurs;
  private List<LineCustomerOrder> ligneCommandeClients;
  private List<InventoryMovement> mvtStks;
}
