package com.skysoft.app.domain.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Company {

    private Long id;
    private String nom;
    private String description;
    private Address adresse;
    private String codeFiscal;
    private String photo;
    private String email;
    private String numTel;
    private String siteWeb;

}
