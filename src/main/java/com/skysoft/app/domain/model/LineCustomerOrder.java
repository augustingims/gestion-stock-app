package com.skysoft.app.domain.model;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LineCustomerOrder {
    private Long id;
    private Product article;
    private CustomerOrder commandeClient;
    private BigDecimal quantite;
    private BigDecimal prixUnitaire;
    private Long idEntreprise;
}
