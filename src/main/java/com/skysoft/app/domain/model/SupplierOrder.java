package com.skysoft.app.domain.model;

import com.skysoft.app.domain.model.enumeration.OrderStatus;
import java.time.Instant;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SupplierOrder {
    private Long id;
    private String code;
    private Instant dateCommande;
    private OrderStatus etatCommande;
    private Supplier fournisseur;
    private Long idEntreprise;
    private List<LineSupplierOrder> ligneCommandeFournisseurs;
    public boolean isDeliveredOrder() {
        return OrderStatus.LIVREE.equals(this.etatCommande);
    }

}
