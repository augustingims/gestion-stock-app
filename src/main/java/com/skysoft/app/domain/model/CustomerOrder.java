package com.skysoft.app.domain.model;

import com.skysoft.app.domain.model.enumeration.OrderStatus;
import java.time.Instant;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerOrder {
    private Long id;
    private String code;
    private Instant dateCommande;
    private OrderStatus etatCommande;
    private Customer client;
    private Long idEntreprise;
    private List<LineCustomerOrder> ligneCommandeClients;

    public boolean isDeliveredOrder() {
        return OrderStatus.LIVREE.equals(this.etatCommande);
    }
}
