package com.skysoft.app.domain.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Supplier {
    private Long id;
    private String nom;
    private String prenom;
    private Address adresse;
    private String photo;
    private String mail;
    private String numTel;
    private Long idEntreprise;
}
