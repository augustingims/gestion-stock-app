package com.skysoft.app.domain.model;

import java.time.Instant;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Sales {
    private Long id;
    private String code;
    private Instant dateVente;
    private String commentaire;
    private List<LineSales> ligneVentes;
    private Long idEntreprise;
}
