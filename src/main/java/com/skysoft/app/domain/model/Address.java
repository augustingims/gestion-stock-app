package com.skysoft.app.domain.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Address {
    private String adresse1;
    private String adresse2;
    private String ville;
    private String pays;
    private String codePostale;
}
