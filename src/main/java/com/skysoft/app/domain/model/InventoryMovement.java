package com.skysoft.app.domain.model;

import com.skysoft.app.domain.model.enumeration.SourceInventoryMovement;
import com.skysoft.app.domain.model.enumeration.TypeInventoryMovement;
import java.math.BigDecimal;
import java.time.Instant;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InventoryMovement {
    private Long id;
    private Instant dateMvt;
    private BigDecimal quantite;
    private Product article;
    private TypeInventoryMovement typeMvtStk;
    private SourceInventoryMovement sourceMvtStk;
    private Long idEntreprise;
}
