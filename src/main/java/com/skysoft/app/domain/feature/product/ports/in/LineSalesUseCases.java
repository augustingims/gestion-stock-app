package com.skysoft.app.domain.feature.product.ports.in;

import com.skysoft.app.domain.model.*;
import java.util.List;

public interface LineSalesUseCases {
  List<LineSales> findHistoriqueVentes(Long productId);
}
