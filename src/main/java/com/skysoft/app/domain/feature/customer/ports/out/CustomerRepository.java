package com.skysoft.app.domain.feature.customer.ports.out;

import com.skysoft.app.domain.model.Customer;
import java.util.List;
import java.util.Optional;

public interface CustomerRepository {
  Customer saveCustomer(Customer dto);

  Optional<Customer> findCustomerById(Long id);

  List<Customer> findAllCustomer();

  void deleteCustomer(Long id);
}
