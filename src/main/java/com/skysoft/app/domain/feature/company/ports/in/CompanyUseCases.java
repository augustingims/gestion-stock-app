package com.skysoft.app.domain.feature.company.ports.in;

import com.skysoft.app.domain.model.Company;
import java.util.List;
import java.util.Optional;

public interface CompanyUseCases {
  Company createCompany(Company company, String password);

  Optional<Company> getCompany(Long companyId);

  List<Company> getAllCompany();

  Optional<Company> updateCompany(Company company);
}
