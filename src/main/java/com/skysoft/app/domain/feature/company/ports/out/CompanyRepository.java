package com.skysoft.app.domain.feature.company.ports.out;

import com.skysoft.app.domain.model.Company;
import java.util.List;
import java.util.Optional;

public interface CompanyRepository {
  Company saveCompany(Company company);

  Optional<Company> findCompany(Long companyId);

  List<Company> findAllCompany();
}
