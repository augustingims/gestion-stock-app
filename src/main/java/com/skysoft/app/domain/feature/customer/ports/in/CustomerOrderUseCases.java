package com.skysoft.app.domain.feature.customer.ports.in;

import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.model.CustomerOrder;
import com.skysoft.app.domain.model.enumeration.OrderStatus;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface CustomerOrderUseCases {
  CustomerOrder createCustomerOrder(CustomerOrder customerOrder) throws TechnicalErrorException;

  CustomerOrder updateEtatCommande(Long idCommande, OrderStatus etatCommande)
      throws TechnicalErrorException;

  CustomerOrder updateQuantiteCommande(Long idCommande, Long idLigneCommande, BigDecimal quantite)
      throws TechnicalErrorException;

  CustomerOrder updateClient(Long idCommande, Long idClient) throws TechnicalErrorException;

  CustomerOrder updateArticle(Long idCommande, Long idLigneCommande, Long newIdArticle)
      throws TechnicalErrorException;

  CustomerOrder deleteArticle(Long idCommande, Long idLigneCommande) throws TechnicalErrorException;

  Optional<CustomerOrder> getCustomerOrderById(Long customerOrderId) throws TechnicalErrorException;

  Optional<CustomerOrder> getCustomerOrderByCode(String customerOrderCode)
      throws TechnicalErrorException;

  List<CustomerOrder> getAllCustomerOrder();

  void deleteCustomerOrder(Long customerOrderId);
}
