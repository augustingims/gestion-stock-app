package com.skysoft.app.domain.feature.product.ports.out;

import com.skysoft.app.domain.model.*;
import java.util.List;
import java.util.Optional;

public interface CategoryRepository {

  Category saveCategory(Category dto);

  Optional<Category> findCategoryById(Long categoryId);

  Optional<Category> findCategoryByCode(String code);

  List<Category> findAllCategory();

  void deleteCategory(Long categoryId);
}
