package com.skysoft.app.domain.feature.product.service;

import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.product.ports.in.CategoryUseCases;
import com.skysoft.app.domain.feature.product.ports.out.CategoryRepository;
import com.skysoft.app.domain.feature.product.ports.out.ProductRepository;
import com.skysoft.app.domain.model.Category;
import com.skysoft.app.domain.model.Product;
import com.skysoft.app.domain.validator.CategoryValidator;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Slf4j
@RequiredArgsConstructor
public class CategoryService implements CategoryUseCases {
  private final CategoryRepository categoryRepository;
  private final ProductRepository productRepository;

  @Override
  public Category createCategory(final Category category) {
    final List<String> errors = CategoryValidator.validate(category);
    if (!errors.isEmpty()) {
      log.error("Category is not valid {}", category);
      throw new FunctionalErrorException(
          "La categorie n'est pas valide", ErrorCodes.CATEGORY_NOT_VALID, errors);
    }
    category.setIdEntreprise(Long.valueOf(MDC.get("idEntreprise")));
    return categoryRepository.saveCategory(category);
  }

  @Override
  public Optional<Category> getCategoryById(final Long categoryId) throws TechnicalErrorException {
    if (categoryId == null) {
      log.error("Categorie ID is null");
      return Optional.empty();
    }
    return Optional.ofNullable(
        categoryRepository
            .findCategoryById(categoryId)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        "Aucune categorie avec l'ID = " + categoryId + " n' ete trouve dans la BDD",
                        ErrorCodes.CATEGORY_NOT_FOUND)));
  }

  @Override
  public Optional<Category> getCategoryByCode(final String code) throws TechnicalErrorException {
    if (!StringUtils.hasLength(code)) {
      log.error("Categorie CODE is null");
      return Optional.empty();
    }
    return Optional.ofNullable(
        categoryRepository
            .findCategoryByCode(code)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        "Aucune categorie avec le CODE = " + code + " n' ete trouve dans la BDD",
                        ErrorCodes.CATEGORY_NOT_FOUND)));
  }

  @Override
  public List<Category> getAllCategory() {
    return categoryRepository.findAllCategory();
  }

  @Override
  public void deleteCategory(final Long categoryId) {
    if (categoryId == null) {
      log.error("Categorie ID is null");
      return;
    }
    final List<Product> products = productRepository.findAllArticleByIdCategory(categoryId);
    if (!products.isEmpty()) {
      throw new FunctionalErrorException(
          "Impossible de supprimer cette categorie qui est deja utilise",
          ErrorCodes.CATEGORY_ALREADY_IN_USE);
    }
    categoryRepository.deleteCategory(categoryId);
  }
}
