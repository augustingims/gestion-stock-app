package com.skysoft.app.domain.feature.supplier.ports.out;

import com.skysoft.app.domain.model.Supplier;
import java.util.List;
import java.util.Optional;

public interface SupplierRepository {
  Supplier saveSupplier(Supplier supplier);

  Optional<Supplier> findSupplierById(Long supplierId);

  List<Supplier> findAllSupplier();

  void deleteSupplier(Long supplierId);
}
