package com.skysoft.app.domain.feature.supplier.ports.in;

import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.model.Supplier;
import java.util.List;
import java.util.Optional;

public interface SupplierUseCases {
  Supplier createSupplier(Supplier supplier);

  Optional<Supplier> getSupplierById(Long supplierId) throws TechnicalErrorException;

  List<Supplier> getAllSupplier();

  void deleteSupplier(Long supplierId);
}
