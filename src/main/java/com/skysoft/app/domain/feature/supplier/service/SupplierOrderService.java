package com.skysoft.app.domain.feature.supplier.service;

import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.product.ports.out.InventoryMovementRepository;
import com.skysoft.app.domain.feature.product.ports.out.ProductRepository;
import com.skysoft.app.domain.feature.supplier.ports.in.SupplierOrderUseCases;
import com.skysoft.app.domain.feature.supplier.ports.out.LineSupplierOrderRepository;
import com.skysoft.app.domain.feature.supplier.ports.out.SupplierOrderRepository;
import com.skysoft.app.domain.feature.supplier.ports.out.SupplierRepository;
import com.skysoft.app.domain.model.*;
import com.skysoft.app.domain.model.enumeration.OrderStatus;
import com.skysoft.app.domain.model.enumeration.SourceInventoryMovement;
import com.skysoft.app.domain.model.enumeration.TypeInventoryMovement;
import com.skysoft.app.domain.utils.MessageUtils;
import com.skysoft.app.domain.validator.SupplierOrderValidator;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Slf4j
@RequiredArgsConstructor
public class SupplierOrderService implements SupplierOrderUseCases {
  private final SupplierOrderRepository supplierOrderRepository;
  private final LineSupplierOrderRepository lineSupplierOrderRepository;
  private final SupplierRepository supplierRepository;
  private final ProductRepository productRepository;
  private final InventoryMovementRepository inventoryMovementRepository;

  @Override
  public SupplierOrder createSupplierOrder(SupplierOrder supplierOrder)
      throws TechnicalErrorException {
    List<String> errors = SupplierOrderValidator.validate(supplierOrder);
    if (!errors.isEmpty()) {
      log.error("La commande fournisseur n'est pas valide {}", supplierOrder);
      throw new FunctionalErrorException(
          "La commande fournisseur n'est pas valide",
          ErrorCodes.COMMANDE_FOURNISSEUR_NOT_VALID,
          errors);
    }
    if (supplierOrder.getId() != null && supplierOrder.isDeliveredOrder()) {
      throw new FunctionalErrorException(
          "Impossible de modifier la commande lorsqu'elle est livree",
          ErrorCodes.COMMANDE_FOURNISSEUR_NON_MODIFIABLE);
    }

    Optional<Supplier> fournisseur =
        supplierRepository.findSupplierById(supplierOrder.getFournisseur().getId());
    if (fournisseur.isEmpty()) {
      log.error("Le fournisseur n'a pas ete trouve dans la BD");
      throw new TechnicalErrorException(
          String.format(
              MessageUtils.MESSAGE_M,
              "fournisseur",
              "l'ID",
              supplierOrder.getFournisseur().getId()),
          ErrorCodes.FOURNISSEUR_NOT_FOUND);
    }

    List<String> articleErrors = new ArrayList<>();
    if (supplierOrder.getLigneCommandeFournisseurs() != null) {
      supplierOrder
          .getLigneCommandeFournisseurs()
          .forEach(
              ligneCommande -> {
                if (ligneCommande.getArticle() != null) {
                  Optional<Product> article =
                      productRepository.findArticleById(ligneCommande.getArticle().getId());
                  if (article.isEmpty()) {
                    articleErrors.add(
                        String.format(
                            MessageUtils.MESSAGE_M,
                            "article",
                            "l'ID",
                            ligneCommande.getArticle().getId().toString()));
                  }
                } else {
                  articleErrors.add("Impossible d'enregistrer une commande avec un article null");
                }
              });
    }

    if (!articleErrors.isEmpty()) {
      log.error("L'article n'existe pas dans la BD");
      throw new FunctionalErrorException(
          "L'article n'existe pas dans la BD", ErrorCodes.ARTICLE_NOT_FOUND);
    }
    supplierOrder.setDateCommande(Instant.now());

    SupplierOrder savedCommandeFournisseur =
        supplierOrderRepository.saveSupplierOrder(supplierOrder);

    if (supplierOrder.getLigneCommandeFournisseurs() != null) {
      supplierOrder
          .getLigneCommandeFournisseurs()
          .forEach(
              ligneCommande -> {
                ligneCommande.setCommandeFournisseur(savedCommandeFournisseur);
                ligneCommande.setIdEntreprise(supplierOrder.getIdEntreprise());
                LineSupplierOrder savedLigneCommandeFournisseur =
                    lineSupplierOrderRepository.saveLineSupplierOrder(ligneCommande);

                effectuerEntree(savedLigneCommandeFournisseur);
              });
    }

    return savedCommandeFournisseur;
  }

  @Override
  public SupplierOrder updateEtatCommande(Long idCommande, OrderStatus etatCommande)
      throws TechnicalErrorException {
    checkIdCommande(idCommande);
    if (!StringUtils.hasText(String.valueOf(etatCommande))) {
      log.error("L'etat de la commande fournisseur est null");
      throw new FunctionalErrorException(
          "Impossible de modifier l'etat de la commande avec un etat null",
          ErrorCodes.COMMANDE_FOURNISSEUR_NON_MODIFIABLE);
    }
    SupplierOrder commandeFournisseurDto = checkEtatCommande(idCommande);
    commandeFournisseurDto.setEtatCommande(etatCommande);

    SupplierOrder savedCommandeFournisseur =
        supplierOrderRepository.saveSupplierOrder(commandeFournisseurDto);

    if (commandeFournisseurDto.isDeliveredOrder()) {
      updateMvtStock(idCommande);
    }
    return savedCommandeFournisseur;
  }

  @Override
  public SupplierOrder updateQuantiteCommande(
      Long idCommande, Long idLigneCommande, BigDecimal quantite) throws TechnicalErrorException {
    checkIdCommande(idCommande);
    checkIdLigneCommande(idLigneCommande);

    if (quantite == null || quantite.compareTo(BigDecimal.ZERO) == 0) {
      log.error("La quantite est 0");
      throw new FunctionalErrorException(
          "Impossible de modifier la quantite de la commande avec 0",
          ErrorCodes.COMMANDE_FOURNISSEUR_NON_MODIFIABLE);
    }

    SupplierOrder commandeFournisseurDto = checkEtatCommande(idCommande);
    Optional<LineSupplierOrder> ligneCommandeFournisseurOptional =
        findLigneCommandeFournisseur(idLigneCommande);

    ligneCommandeFournisseurOptional.ifPresent(
        ligneCommandeFournisseur -> {
          ligneCommandeFournisseur.setQuantite(quantite);
          lineSupplierOrderRepository.saveLineSupplierOrder(ligneCommandeFournisseur);
        });

    return commandeFournisseurDto;
  }

  @Override
  public SupplierOrder updateFournisseur(Long idCommande, Long idfournisseur)
      throws TechnicalErrorException {
    checkIdCommande(idCommande);
    if (null == idfournisseur) {
      log.error("l'id du fournisseur est null");
      throw new FunctionalErrorException(
          "Impossible de modifier la commande avec l'id du fournisseur null",
          ErrorCodes.COMMANDE_FOURNISSEUR_NON_MODIFIABLE);
    }
    SupplierOrder commandeFournisseurDto = checkEtatCommande(idCommande);
    Optional<Supplier> fournisseurOptional = supplierRepository.findSupplierById(idfournisseur);
    if (fournisseurOptional.isEmpty()) {
      throw new TechnicalErrorException(
          String.format(MessageUtils.MESSAGE_M, "fournisseur", "l'ID", idfournisseur),
          ErrorCodes.FOURNISSEUR_NOT_FOUND);
    }
    commandeFournisseurDto.setFournisseur(fournisseurOptional.get());
    return supplierOrderRepository.saveSupplierOrder(commandeFournisseurDto);
  }

  @Override
  public SupplierOrder updateArticleInSupplierOrder(
      Long idCommande, Long idLigneCommande, Long idArticle) throws TechnicalErrorException {
    checkIdCommande(idCommande);
    checkIdLigneCommande(idLigneCommande);

    if (null == idArticle) {
      log.error("L'id de l'article est null");
      throw new FunctionalErrorException(
          "Impossible de modifier la commade avec l'id de l'article null",
          ErrorCodes.COMMANDE_FOURNISSEUR_NON_MODIFIABLE);
    }

    SupplierOrder commandeFournisseurDto = checkEtatCommande(idCommande);
    Optional<LineSupplierOrder> ligneCommandeFournisseurOptional =
        findLigneCommandeFournisseur(idLigneCommande);
    Optional<Product> articleOptional = productRepository.findArticleById(idArticle);
    if (articleOptional.isEmpty()) {
      throw new TechnicalErrorException(
          String.format(MessageUtils.MESSAGE_M, "article", "l'ID", idArticle),
          ErrorCodes.ARTICLE_NOT_FOUND);
    }

    ligneCommandeFournisseurOptional.ifPresent(
        ligneCommandeFournisseur -> {
          ligneCommandeFournisseur.setArticle(articleOptional.get());
          lineSupplierOrderRepository.saveLineSupplierOrder(ligneCommandeFournisseur);
        });

    return commandeFournisseurDto;
  }

  @Override
  public SupplierOrder deleteArticleInSupplierOrder(Long idCommande, Long idLigneCommande)
      throws TechnicalErrorException {
    checkIdCommande(idCommande);
    checkIdLigneCommande(idLigneCommande);

    SupplierOrder commentfournisseurDto = checkEtatCommande(idCommande);
    findLigneCommandeFournisseur(idLigneCommande);
    lineSupplierOrderRepository.deleteLineSupplierOrder(idLigneCommande);

    return commentfournisseurDto;
  }

  @Override
  public Optional<SupplierOrder> getSupplierOrderById(Long id) throws TechnicalErrorException {
    if (null == id) {
      log.error("L'id de la commande fournisseur ne doit etre null");
      return Optional.empty();
    }
    return Optional.ofNullable(
        supplierOrderRepository
            .findSupplierOrderById(id)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        String.format(MessageUtils.MESSAGE_F, "commande fournisseur", "l'id", id),
                        ErrorCodes.COMMANDE_FOURNISSEUR_NOT_FOUND)));
  }

  @Override
  public Optional<SupplierOrder> getSupplierOrderByCode(String code)
      throws TechnicalErrorException {
    if (!StringUtils.hasText(code)) {
      log.error("Le code de la commande fournisseur ne doit etre null");
      return Optional.empty();
    }
    return Optional.ofNullable(
        supplierOrderRepository
            .findSupplierOrderByCode(code)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        String.format(
                            MessageUtils.MESSAGE_F, "commande fournisseur", "le code", code),
                        ErrorCodes.COMMANDE_FOURNISSEUR_NOT_FOUND)));
  }

  @Override
  public List<SupplierOrder> getAllSupplierOrder() {
    return supplierOrderRepository.findAllSupplierOrder();
  }

  @Override
  public void deleteSupplierOrder(Long id) {
    if (null == id) {
      log.error("L'id de la commande fournisseur ne doit etre null");
      return;
    }
    List<LineSupplierOrder> ligneCommandeFournisseurs =
        lineSupplierOrderRepository.findAllLignesCommandesFournisseurByCommandeFournisseurId(id);
    if (!ligneCommandeFournisseurs.isEmpty()) {
      throw new FunctionalErrorException(
          "Impossible de supprimer une commande fournisseur deja utilisee",
          ErrorCodes.COMMANDE_FOURNISSEUR_ALREADY_IN_USE);
    }
    supplierOrderRepository.deleteSupplierOrder(id);
  }

  private void updateMvtStock(Long idCommande) {
    List<LineSupplierOrder> list =
        lineSupplierOrderRepository.findAllLignesCommandesFournisseurByCommandeFournisseurId(
            idCommande);
    list.forEach(this::effectuerEntree);
  }

  private SupplierOrder checkEtatCommande(Long idCommande) throws TechnicalErrorException {
    SupplierOrder supplierOrder =
        getSupplierOrderById(idCommande)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        String.format(
                            MessageUtils.MESSAGE_F, "commande fournisseur", "l'id", idCommande),
                        ErrorCodes.COMMANDE_FOURNISSEUR_NOT_FOUND));
    if (supplierOrder.isDeliveredOrder()) {
      throw new FunctionalErrorException(
          "Impossible de modifier la commande lorsqu'elle est livree",
          ErrorCodes.COMMANDE_FOURNISSEUR_NON_MODIFIABLE);
    }
    return supplierOrder;
  }

  private Optional<LineSupplierOrder> findLigneCommandeFournisseur(Long idLigneCommande)
      throws TechnicalErrorException {
    Optional<LineSupplierOrder> ligneCommandeFournisseurOptional =
        lineSupplierOrderRepository.findByLineSupplierOrderId(idLigneCommande);
    if (ligneCommandeFournisseurOptional.isEmpty()) {
      throw new TechnicalErrorException(
          String.format(
              MessageUtils.MESSAGE_F,
              "ligne commande fournisseur",
              "l'ID",
              idLigneCommande.toString()),
          ErrorCodes.LIGNE_COMMANDE_FOURNISSEUR_NOT_FOUND);
    }
    return ligneCommandeFournisseurOptional;
  }

  private void checkIdCommande(Long idCommande) {
    if (null == idCommande) {
      log.error("L'id de la commande fournisseur ne doit etre null");
      throw new FunctionalErrorException(
          "Impossible de modifier l'etat de la commande avec un id null",
          ErrorCodes.COMMANDE_FOURNISSEUR_NON_MODIFIABLE);
    }
  }

  private void checkIdLigneCommande(Long idLigneCommande) {
    if (null == idLigneCommande) {
      log.error("L'id de la ligne commande fournisseur ne doit etre null");
      throw new FunctionalErrorException(
          "Impossible de modifier l'etat de la commande avec un id ligne commande null",
          ErrorCodes.COMMANDE_FOURNISSEUR_NON_MODIFIABLE);
    }
  }

  private void effectuerEntree(LineSupplierOrder lineSupplierOrder) {
    InventoryMovement mvtStkDto =
        InventoryMovement.builder()
            .article(lineSupplierOrder.getArticle())
            .dateMvt(Instant.now())
            .typeMvtStk(TypeInventoryMovement.ENTREE)
            .sourceMvtStk(SourceInventoryMovement.COMMANDE_FOURNISSEUR)
            .quantite(lineSupplierOrder.getQuantite())
            .idEntreprise(lineSupplierOrder.getIdEntreprise())
            .build();

    inventoryMovementRepository.saveInventoryMovement(mvtStkDto);
  }
}
