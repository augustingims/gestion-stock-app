package com.skysoft.app.domain.feature.product.ports.out;

import com.skysoft.app.domain.model.*;
import java.util.List;
import java.util.Optional;

public interface ProductRepository {
  Product saveArticle(Product product);

  Optional<Product> findArticleById(Long productId);

  Optional<Product> findByCodeArticle(String codeProduct);

  List<Product> findAllArticleByIdCategory(Long categoryId);

  List<Product> findByCriteria(final Long productId, final String productCode);

  void deleteArticle(Long productId);
}
