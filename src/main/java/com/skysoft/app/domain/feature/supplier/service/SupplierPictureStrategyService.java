package com.skysoft.app.domain.feature.supplier.service;

import com.skysoft.app.domain.model.Supplier;
import com.skysoft.app.domain.service.PictureStrategy;
import java.io.InputStream;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service("supplierPictureStrategyService")
@Slf4j
@RequiredArgsConstructor
public class SupplierPictureStrategyService implements PictureStrategy<Supplier> {
  @Override
  public Optional<Supplier> upload(final Long id, final InputStream photo, final String titre) {
    return Optional.empty();
  }
}
