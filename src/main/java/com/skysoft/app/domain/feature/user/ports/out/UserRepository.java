package com.skysoft.app.domain.feature.user.ports.out;

import com.skysoft.app.domain.model.Authority;
import com.skysoft.app.domain.model.User;
import java.util.List;
import java.util.Optional;

public interface UserRepository {
  User saveUser(User user);

  Optional<User> findById(Long userId);

  Optional<User> findByEmail(String email);

  Optional<User> currentUser();

  List<User> findByCriteria(Long userId, String email);

  void saveAuthority(Authority authority);
}
