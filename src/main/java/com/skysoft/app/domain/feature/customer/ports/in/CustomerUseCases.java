package com.skysoft.app.domain.feature.customer.ports.in;

import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.model.Customer;
import java.util.List;
import java.util.Optional;

public interface CustomerUseCases {
  Customer createCustomer(Customer customer);

  Optional<Customer> getCustomerById(Long id) throws TechnicalErrorException;

  List<Customer> getAllCustomer();

  void deleteCustomer(Long id);
}
