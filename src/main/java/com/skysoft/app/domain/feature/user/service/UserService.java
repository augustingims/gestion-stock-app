package com.skysoft.app.domain.feature.user.service;

import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.user.ports.in.UserUseCases;
import com.skysoft.app.domain.feature.user.ports.out.UserRepository;
import com.skysoft.app.domain.model.User;
import com.skysoft.app.domain.utils.MessageUtils;
import com.skysoft.app.domain.validator.UserValidator;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService implements UserUseCases {
  private final UserRepository userRepository;

  @Override
  public User createUser(final User user) {
    final List<String> errors = UserValidator.validate(user);
    if (!errors.isEmpty()) {
      log.error("L'utilisateur n'est pas valide {}", user);
      throw new FunctionalErrorException(
          "L'utilisateur n'est pas valide", ErrorCodes.UTILISATEUR_NOT_VALID, errors);
    }

    if (userAlreadyExists(user.getEmail())) {
      throw new FunctionalErrorException(
          "Un autre utilisateur avec le meme email existe deja",
          ErrorCodes.UTILISATEUR_ALREADY_EXISTS,
          Collections.singletonList(
              "Un autre utilisateur avec le meme email existe deja dans la BD"));
    }
    return userRepository.saveUser(user);
  }

  private boolean userAlreadyExists(final String email) {
    final Optional<User> user = userRepository.findByEmail(email);
    return user.isPresent();
  }

  @Override
  public Optional<User> getById(final Long userId) throws TechnicalErrorException {
    if (null == userId) {
      log.error("l'id de utilisateur ne doit pas etre null");
      return Optional.empty();
    }
    return Optional.ofNullable(
        userRepository
            .findById(userId)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        String.format(MessageUtils.MESSAGE_M, "utilisateur", "l'id", userId),
                        ErrorCodes.UTILISATEUR_NOT_FOUND)));
  }

  @Override
  public Optional<User> getByEmail(final String email) throws TechnicalErrorException {
    return Optional.ofNullable(
        userRepository
            .findByEmail(email)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        String.format(MessageUtils.MESSAGE_M, "utilisateur", "l'email", email),
                        ErrorCodes.UTILISATEUR_NOT_FOUND)));
  }

  @Override
  public User changePassword(final Long id, final String password, final String confirmPassword) {
    validate(id, password, confirmPassword);
    final Optional<User> utilisateurOptional = userRepository.findById(id);
    if (!utilisateurOptional.isPresent()) {
      log.error("Aucun utilisateur n'a pas ete trouve avec l'ID" + id);
      throw new FunctionalErrorException(
          String.format(MessageUtils.MESSAGE_M, "utilisateur", "l'ID", id),
          ErrorCodes.ENTREPRISE_NOT_FOUND);
    }
    final User userUpdate = utilisateurOptional.get();
    userUpdate.setMotDePasse(password);
    return userRepository.saveUser(userUpdate);
  }

  @Override
  public Optional<User> getCurrentUser() {
    return userRepository.currentUser();
  }

  @Override
  public List<User> getAllUsers(final Long id, final String email) {
    return userRepository.findByCriteria(id,email);
  }

  private void validate(final Long id, final String password, final String confirmPassword) {
    if (null == id && null == password && null == confirmPassword) {
      log.warn("Impossible de modifier le mot de passe avec un object NULL");
      throw new FunctionalErrorException(
          "Aucune information n'a ete fourni pour pouvoir changer le mot de passe",
          ErrorCodes.UTILISATEUR_CHANGE_PASSWORD_OBJECT_NOT_VALID);
    }

    if (null == id) {
      log.warn("Impossible de modifier le mot de passe avec un ID NULL");
      throw new FunctionalErrorException(
          "l'id de l'utilisateur est null, Impossible de modifier le mot de passe",
          ErrorCodes.UTILISATEUR_CHANGE_PASSWORD_OBJECT_NOT_VALID);
    }

    if (!StringUtils.hasText(password) || !StringUtils.hasText(confirmPassword)) {
      log.warn("Impossible de modifier le mot de passe avec un mot de passe NULL");
      throw new FunctionalErrorException(
          "Le mot de passe de l'utilisateur est null, Impossible de modifier le mot de passe",
          ErrorCodes.UTILISATEUR_CHANGE_PASSWORD_OBJECT_NOT_VALID);
    }

    if (!password.equals(confirmPassword)) {
      log.warn("Impossible de modifier le mot de passe avec deux mots de passe different");
      throw new FunctionalErrorException(
          "Mots de passe utilisateur non conformes, Impossible de mot de passe",
          ErrorCodes.UTILISATEUR_CHANGE_PASSWORD_OBJECT_NOT_VALID);
    }
  }
}
