package com.skysoft.app.domain.feature.customer.service;

import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.customer.ports.in.CustomerOrderUseCases;
import com.skysoft.app.domain.feature.customer.ports.out.CustomerOrderRepository;
import com.skysoft.app.domain.feature.customer.ports.out.CustomerRepository;
import com.skysoft.app.domain.feature.customer.ports.out.LineCustomerOrderRepository;
import com.skysoft.app.domain.feature.product.ports.out.InventoryMovementRepository;
import com.skysoft.app.domain.feature.product.ports.out.ProductRepository;
import com.skysoft.app.domain.model.*;
import com.skysoft.app.domain.model.enumeration.OrderStatus;
import com.skysoft.app.domain.model.enumeration.SourceInventoryMovement;
import com.skysoft.app.domain.model.enumeration.TypeInventoryMovement;
import com.skysoft.app.domain.utils.MessageUtils;
import com.skysoft.app.domain.validator.CustomerOrderValidator;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Slf4j
@RequiredArgsConstructor
public class CustomerOrderService implements CustomerOrderUseCases {
  private final CustomerOrderRepository customerOrderRepository;
  private final LineCustomerOrderRepository lineCustomerOrderRepository;
  private final InventoryMovementRepository inventoryMovementRepository;
  private final ProductRepository productRepository;
  private final CustomerRepository customerRepository;

  @Override
  public CustomerOrder createCustomerOrder(CustomerOrder customerOrder)
      throws TechnicalErrorException {
    List<String> errors = CustomerOrderValidator.validate(customerOrder);

    if (!errors.isEmpty()) {
      log.error("Commande client n'est pas valide");
      throw new FunctionalErrorException(
          "La commande client n'est pas valide", ErrorCodes.COMMANDE_CLIENT_NOT_VALID, errors);
    }

    if (customerOrder.getId() != null && customerOrder.isDeliveredOrder()) {
      throw new FunctionalErrorException(
          "Impossible de modifier la commande lorsqu'elle est livree",
          ErrorCodes.COMMANDE_CLIENT_NON_MODIFIABLE);
    }

    Optional<Customer> client =
        customerRepository.findCustomerById(customerOrder.getClient().getId());
    if (client.isEmpty()) {
      log.warn("Client with ID {} was not found in the DB", customerOrder.getClient().getId());
      throw new TechnicalErrorException(
          "Aucun client avec l'ID"
              + customerOrder.getClient().getId()
              + " n'a ete trouve dans la BDD",
          ErrorCodes.CLIENT_NOT_FOUND);
    }

    List<String> articleErrors = new ArrayList<>();

    if (customerOrder.getLigneCommandeClients() != null) {
      customerOrder
          .getLigneCommandeClients()
          .forEach(
              ligCmdClt -> {
                if (ligCmdClt.getArticle() != null) {
                  Optional<Product> article =
                      productRepository.findArticleById(ligCmdClt.getArticle().getId());
                  if (article.isEmpty()) {
                    articleErrors.add(
                        "L'article avec l'ID " + ligCmdClt.getArticle().getId() + " n'existe pas");
                  }
                } else {
                  articleErrors.add("Impossible d'enregister une commande avec un aticle NULL");
                }
              });
    }

    if (!articleErrors.isEmpty()) {
      throw new FunctionalErrorException(
          "Article n'existe pas dans la BDD", ErrorCodes.ARTICLE_NOT_FOUND, articleErrors);
    }
    customerOrder.setDateCommande(Instant.now());
    CustomerOrder savedCmdClt = customerOrderRepository.saveCustomerOrder(customerOrder);

    if (customerOrder.getLigneCommandeClients() != null) {
      customerOrder
          .getLigneCommandeClients()
          .forEach(
              ligCmdClt -> {
                LineCustomerOrder ligneCommandeClient = LineCustomerOrder.builder().build();
                ligneCommandeClient.setCommandeClient(savedCmdClt);
                ligneCommandeClient.setIdEntreprise(customerOrder.getIdEntreprise());
                LineCustomerOrder savedLigneCmd =
                    lineCustomerOrderRepository.saveLineCustomerOrder(ligneCommandeClient);

                effectuerSortie(savedLigneCmd);
              });
    }

    return savedCmdClt;
  }

  private void effectuerSortie(LineCustomerOrder savedLigneCommandeClient) {
    InventoryMovement mvtStkDto =
        InventoryMovement.builder()
            .article(savedLigneCommandeClient.getArticle())
            .dateMvt(Instant.now())
            .typeMvtStk(TypeInventoryMovement.SORTIE)
            .sourceMvtStk(SourceInventoryMovement.COMMANDE_CLIENT)
            .quantite(savedLigneCommandeClient.getQuantite())
            .idEntreprise(savedLigneCommandeClient.getIdEntreprise())
            .build();

    inventoryMovementRepository.saveInventoryMovement(mvtStkDto);
  }

  @Override
  public CustomerOrder updateEtatCommande(Long idCommande, OrderStatus etatCommande)
      throws TechnicalErrorException {
    checkIdCommande(idCommande);
    if (!StringUtils.hasText(String.valueOf(etatCommande))) {
      log.error("L'etat de la commande client est null");
      throw new FunctionalErrorException(
          "Impossible de modifier l'etat de la commande avec un etat null",
          ErrorCodes.COMMANDE_CLIENT_NON_MODIFIABLE);
    }
    CustomerOrder commandeClientDto = checkEtatCommande(idCommande);
    commandeClientDto.setEtatCommande(etatCommande);

    CustomerOrder savedCommandeClient =
        customerOrderRepository.saveCustomerOrder(commandeClientDto);

    if (commandeClientDto.isDeliveredOrder()) {
      updateMvtStock(idCommande);
    }
    return savedCommandeClient;
  }

  private void updateMvtStock(Long idCommande) {
    List<LineCustomerOrder> list =
        lineCustomerOrderRepository.findAllLignesCommandesClientByCommandeClientId(idCommande);
    list.forEach(this::effectuerSortie);
  }

  private CustomerOrder checkEtatCommande(Long idCommande) throws TechnicalErrorException {
    CustomerOrder commandeClientDto =
        getCustomerOrderById(idCommande)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        "Aucune commande client n'a ete trouve avec l'ID " + idCommande,
                        ErrorCodes.COMMANDE_CLIENT_NOT_FOUND));

    if (commandeClientDto.isDeliveredOrder()) {
      throw new FunctionalErrorException(
          "Impossible de modifier la commande lorsqu'elle est livree",
          ErrorCodes.COMMANDE_CLIENT_NON_MODIFIABLE);
    }

    return commandeClientDto;
  }

  @Override
  public CustomerOrder updateQuantiteCommande(
      Long idCommande, Long idLigneCommande, BigDecimal quantite) throws TechnicalErrorException {
    checkIdCommande(idCommande);
    checkIdLigneCommande(idLigneCommande);

    if (quantite == null || quantite.compareTo(BigDecimal.ZERO) == 0) {
      log.error("La quantite est 0");
      throw new FunctionalErrorException(
          "Impossible de modifier la quantite de la commande avec 0",
          ErrorCodes.COMMANDE_CLIENT_NON_MODIFIABLE);
    }

    CustomerOrder commandeClientDto = checkEtatCommande(idCommande);
    Optional<LineCustomerOrder> ligneCommandeClientOptional =
        findLigneCommandeClient(idLigneCommande);

    ligneCommandeClientOptional.ifPresent(
        ligneCommandeClient -> {
          ligneCommandeClient.setQuantite(quantite);
          lineCustomerOrderRepository.saveLineCustomerOrder(ligneCommandeClient);
        });

    return commandeClientDto;
  }

  private Optional<LineCustomerOrder> findLigneCommandeClient(Long idLigneCommande)
      throws TechnicalErrorException {
    Optional<LineCustomerOrder> ligneCommandeClientOptional =
        lineCustomerOrderRepository.findByLineCustomerOrderId(idLigneCommande);
    if (!ligneCommandeClientOptional.isPresent()) {
      throw new TechnicalErrorException(
          String.format(
              MessageUtils.MESSAGE_F, "ligne commande client", "l'ID", idLigneCommande.toString()),
          ErrorCodes.LIGNE_COMMANDE_CLIENT_NOT_FOUND);
    }
    return ligneCommandeClientOptional;
  }

  @Override
  public CustomerOrder updateClient(Long idCommande, Long idClient) throws TechnicalErrorException {
    checkIdCommande(idCommande);
    if (null == idClient) {
      log.error("l'id du client est null");
      throw new FunctionalErrorException(
          "Impossible de modifier la commande avec l'id du client null",
          ErrorCodes.COMMANDE_CLIENT_NON_MODIFIABLE);
    }
    CustomerOrder commandeClientDto = checkEtatCommande(idCommande);
    Optional<Customer> clientOptional = customerRepository.findCustomerById(idClient);
    if (clientOptional.isEmpty()) {
      throw new TechnicalErrorException(
          String.format(MessageUtils.MESSAGE_M, "client", "l'ID", idClient),
          ErrorCodes.CLIENT_NOT_FOUND);
    }
    commandeClientDto.setClient(clientOptional.get());
    return customerOrderRepository.saveCustomerOrder(commandeClientDto);
  }

  @Override
  public CustomerOrder updateArticle(Long idCommande, Long idLigneCommande, Long newIdArticle)
      throws TechnicalErrorException {
    checkIdCommande(idCommande);
    checkIdLigneCommande(idLigneCommande);

    if (null == newIdArticle) {
      log.error("L'id de l'article est null");
      throw new TechnicalErrorException(
          "Impossible de modifier la commade avec l'id de l'article null",
          ErrorCodes.COMMANDE_CLIENT_NON_MODIFIABLE);
    }

    CustomerOrder customerOrder = checkEtatCommande(idCommande);
    Optional<LineCustomerOrder> ligneCommandeClientOptional =
        findLigneCommandeClient(idLigneCommande);
    Optional<Product> articleOptional = productRepository.findArticleById(newIdArticle);
    if (articleOptional.isEmpty()) {
      throw new TechnicalErrorException(
          String.format(MessageUtils.MESSAGE_M, "article", "l'ID", newIdArticle),
          ErrorCodes.ARTICLE_NOT_FOUND);
    }

    ligneCommandeClientOptional.ifPresent(
        ligneCommandeClient -> {
          ligneCommandeClient.setArticle(articleOptional.get());
          lineCustomerOrderRepository.saveLineCustomerOrder(ligneCommandeClient);
        });

    return customerOrder;
  }

  @Override
  public CustomerOrder deleteArticle(Long idCommande, Long idLigneCommande)
      throws TechnicalErrorException {
    checkIdCommande(idCommande);
    checkIdLigneCommande(idLigneCommande);

    CustomerOrder commentClientDto = checkEtatCommande(idCommande);
    findLigneCommandeClient(idLigneCommande);
    lineCustomerOrderRepository.deleteLineCustomerOrder(idLigneCommande);

    return commentClientDto;
  }

  private void checkIdCommande(Long idCommande) {
    if (null == idCommande) {
      log.error("L'id de la commande client ne doit etre null");
      throw new FunctionalErrorException(
          "Impossible de modifier l'etat de la commande avec un id null",
          ErrorCodes.COMMANDE_CLIENT_NON_MODIFIABLE);
    }
  }

  private void checkIdLigneCommande(Long idLigneCommande) {
    if (null == idLigneCommande) {
      log.error("L'id de la ligne commande client ne doit etre null");
      throw new FunctionalErrorException(
          "Impossible de modifier l'etat de la commande avec un id ligne commande null",
          ErrorCodes.COMMANDE_CLIENT_NON_MODIFIABLE);
    }
  }

  @Override
  public Optional<CustomerOrder> getCustomerOrderById(Long customerOrderId)
      throws TechnicalErrorException {
    if (customerOrderId == null) {
      log.error("Commande client ID is NULL");
      return Optional.empty();
    }
    return Optional.ofNullable(
        customerOrderRepository
            .findCustomerOrderById(customerOrderId)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        "Aucune commande client n'a ete trouve avec l'ID " + customerOrderId,
                        ErrorCodes.COMMANDE_CLIENT_NOT_FOUND)));
  }

  @Override
  public Optional<CustomerOrder> getCustomerOrderByCode(String customerOrderCode)
      throws TechnicalErrorException {
    if (!StringUtils.hasLength(customerOrderCode)) {
      log.error("Commande client CODE is NULL");
      return Optional.empty();
    }
    return Optional.ofNullable(
        customerOrderRepository
            .findCustomerOrderByCode(customerOrderCode)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        "Aucune commande client n'a ete trouve avec le CODE " + customerOrderCode,
                        ErrorCodes.COMMANDE_CLIENT_NOT_FOUND)));
  }

  @Override
  public List<CustomerOrder> getAllCustomerOrder() {
    return customerOrderRepository.findAllCustomerOrder();
  }

  @Override
  public void deleteCustomerOrder(Long customerOrderId) {
    if (customerOrderId == null) {
      log.error("Commande client ID is NULL");
      return;
    }
    List<LineCustomerOrder> ligneCommandeClients =
        lineCustomerOrderRepository.findAllLignesCommandesClientByCommandeClientId(customerOrderId);
    if (!ligneCommandeClients.isEmpty()) {
      throw new FunctionalErrorException(
          "Impossible de supprimer une commande client deja utilisee",
          ErrorCodes.COMMANDE_CLIENT_ALREADY_IN_USE);
    }
    customerOrderRepository.deleteCustomerOrder(customerOrderId);
  }
}
