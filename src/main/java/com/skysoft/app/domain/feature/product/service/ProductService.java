package com.skysoft.app.domain.feature.product.service;

import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.customer.ports.out.LineCustomerOrderRepository;
import com.skysoft.app.domain.feature.product.ports.in.ProductUseCases;
import com.skysoft.app.domain.feature.product.ports.out.LineSalesRepository;
import com.skysoft.app.domain.feature.product.ports.out.ProductRepository;
import com.skysoft.app.domain.feature.supplier.ports.out.LineSupplierOrderRepository;
import com.skysoft.app.domain.model.*;
import com.skysoft.app.domain.validator.ProductValidator;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductService implements ProductUseCases {
  private final ProductRepository productRepository;
  private final LineSalesRepository lineSalesRepository;
  private final LineSupplierOrderRepository lineSupplierOrderRepository;
  private final LineCustomerOrderRepository lineCustomerOrderRepository;

  @Override
  public Product createArticle(final Product product) {
    final List<String> errors = ProductValidator.validate(product);
    if (!errors.isEmpty()) {
      log.error("Product is not valid {}", product);
      throw new FunctionalErrorException(
          "L'article n'est pas valide", ErrorCodes.ARTICLE_NOT_VALID, errors);
    }
    return productRepository.saveArticle(product);
  }

  @Override
  public Optional<Product> getArticleById(final Long productId) throws TechnicalErrorException {
    if (productId == null) {
      log.error("Product ID is null");
      return Optional.empty();
    }
    return Optional.ofNullable(
        productRepository
            .findArticleById(productId)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        "Aucun article avec l'ID = " + productId + " n' ete trouve dans la BDD",
                        ErrorCodes.ARTICLE_NOT_FOUND)));
  }

  @Override
  public Optional<Product> getByCodeArticle(final String codeProduct) throws TechnicalErrorException {
    if (!StringUtils.hasLength(codeProduct)) {
      log.error("Product CODE is null");
      return Optional.empty();
    }

    return Optional.ofNullable(
        productRepository
            .findByCodeArticle(codeProduct)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        "Aucun article avec le CODE = "
                            + codeProduct
                            + " n' ete trouve dans la BDD",
                        ErrorCodes.ARTICLE_NOT_FOUND)));
  }

  @Override
  public List<Product> getAllArticleByIdCategory(final Long categoryId) {
    return productRepository.findAllArticleByIdCategory(categoryId);
  }

  @Override
  public void deleteArticle(final Long productId) {
    if (productId == null) {
      log.error("Article ID is null");
      return;
    }
    final List<LineCustomerOrder> ligneCommandeClients =
        lineCustomerOrderRepository.findHistoriaueCommandeClient(productId);
    if (!ligneCommandeClients.isEmpty()) {
      throw new FunctionalErrorException(
          "Impossible de supprimer un article deja utilise dans des commandes client",
          ErrorCodes.ARTICLE_ALREADY_IN_USE);
    }
    final List<LineSupplierOrder> ligneCommandeFournisseurs =
        lineSupplierOrderRepository.findHistoriqueCommandeFournisseur(productId);
    if (!ligneCommandeFournisseurs.isEmpty()) {
      throw new FunctionalErrorException(
          "Impossible de supprimer un article deja utilise dans des commandes fournisseur",
          ErrorCodes.ARTICLE_ALREADY_IN_USE);
    }
    final List<LineSales> ligneVentes = lineSalesRepository.findHistoriqueVentes(productId);
    if (!ligneVentes.isEmpty()) {
      throw new FunctionalErrorException(
          "Impossible de supprimer un article deja utilise dans des ventes",
          ErrorCodes.ARTICLE_ALREADY_IN_USE);
    }
    productRepository.deleteArticle(productId);
  }

  @Override
  public Optional<Product> updateProduct(final Product product) {
    return Optional.ofNullable(productRepository.saveArticle(product));
  }

  @Override
  public List<Product> getAllProducts(final Long id, final String code) {
    return productRepository.findByCriteria(id,code);
  }
}
