package com.skysoft.app.domain.feature.product.ports.out;

import com.skysoft.app.domain.model.*;
import java.math.BigDecimal;
import java.util.List;

public interface InventoryMovementRepository {

  BigDecimal stockReelArticle(Long productId);

  List<InventoryMovement> mvtStkArticle(Long productId);

  InventoryMovement saveInventoryMovement(InventoryMovement inventoryMovement);
}
