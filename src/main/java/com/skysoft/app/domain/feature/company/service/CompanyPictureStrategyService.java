package com.skysoft.app.domain.feature.company.service;

import com.flickr4java.flickr.FlickrException;
import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.model.Company;
import com.skysoft.app.domain.service.PictureStrategy;
import com.skysoft.app.domain.service.out.FlickrService;
import java.io.InputStream;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service("companyPictureStrategyService")
@Slf4j
@RequiredArgsConstructor
public class CompanyPictureStrategyService implements PictureStrategy<Company> {
  private final CompanyService companyService;
  private final FlickrService flickrService;

  @Override
  public Optional<Company> upload(final Long id, final InputStream photo, final String titre)
      throws TechnicalErrorException {
    Optional<Company> company = companyService.getCompany(id);
    if (company.isPresent()) {
      try {
        final String urlPicture = flickrService.savePhoto(photo, titre);
        if (!StringUtils.hasText(urlPicture)) {
          throw new FunctionalErrorException(
              "Erreur lors de l'enregistrement de la photo de l'entreprise",
              ErrorCodes.UPDATE_PHOTO_EXCEPTION);
        }
        company.get().setPhoto(urlPicture);
        company = companyService.updateCompany(company.get());
      } catch (final FlickrException e) {
        throw new TechnicalErrorException(e.getMessage(), e, ErrorCodes.FLICKR_PHOTO_EXCEPTION);
      }
    }
    return company;
  }
}
