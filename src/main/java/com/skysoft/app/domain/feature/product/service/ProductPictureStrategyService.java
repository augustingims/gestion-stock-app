package com.skysoft.app.domain.feature.product.service;

import com.flickr4java.flickr.FlickrException;
import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.model.Product;
import com.skysoft.app.domain.service.PictureStrategy;
import com.skysoft.app.domain.service.out.FlickrService;
import java.io.InputStream;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service("productPictureStrategyService")
@Slf4j
@RequiredArgsConstructor
public class ProductPictureStrategyService implements PictureStrategy<Product> {
  private final ProductService productService;
  private final FlickrService flickrService;

  @Override
  public Optional<Product> upload(final Long id, final InputStream photo, final String titre)
      throws TechnicalErrorException {
    Optional<Product> product = productService.getArticleById(id);
    if (product.isPresent()) {
      try {
        final String urlPicture = flickrService.savePhoto(photo, titre);
        if (!StringUtils.hasText(urlPicture)) {
          throw new FunctionalErrorException(
              "Erreur lors de l'enregistrement de la photo de l'article",
              ErrorCodes.UPDATE_PHOTO_EXCEPTION);
        }
        product.get().setPhoto(urlPicture);
        product = productService.updateProduct(product.get());
      } catch (final FlickrException e) {
        throw new TechnicalErrorException(e.getMessage(), e, ErrorCodes.FLICKR_PHOTO_EXCEPTION);
      }
    }
    return product;
  }
}
