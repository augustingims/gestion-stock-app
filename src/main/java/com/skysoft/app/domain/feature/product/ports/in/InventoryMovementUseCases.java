package com.skysoft.app.domain.feature.product.ports.in;

import com.skysoft.app.domain.model.*;
import java.math.BigDecimal;
import java.util.List;

public interface InventoryMovementUseCases {

  BigDecimal stockReelArticle(Long productId);

  List<InventoryMovement> mvtStkArticle(Long productId);

  InventoryMovement entreeStock(InventoryMovement inventoryMovement);

  InventoryMovement sortieStock(InventoryMovement inventoryMovement);

  InventoryMovement correctionStockPos(InventoryMovement inventoryMovement);

  InventoryMovement correctionStockNeg(InventoryMovement inventoryMovement);
}
