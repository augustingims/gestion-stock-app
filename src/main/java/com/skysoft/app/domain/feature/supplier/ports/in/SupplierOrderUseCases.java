package com.skysoft.app.domain.feature.supplier.ports.in;

import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.model.SupplierOrder;
import com.skysoft.app.domain.model.enumeration.OrderStatus;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface SupplierOrderUseCases {
  SupplierOrder createSupplierOrder(SupplierOrder dto) throws TechnicalErrorException;

  SupplierOrder updateEtatCommande(Long idCommande, OrderStatus etatCommande)
      throws TechnicalErrorException;

  SupplierOrder updateQuantiteCommande(Long idCommande, Long idLigneCommande, BigDecimal quantite)
      throws TechnicalErrorException;

  SupplierOrder updateFournisseur(Long idCommande, Long idFournisseur)
      throws TechnicalErrorException;

  SupplierOrder updateArticleInSupplierOrder(Long idCommande, Long idLigneCommande, Long idArticle)
      throws TechnicalErrorException;

  SupplierOrder deleteArticleInSupplierOrder(Long idCommande, Long idLigneCommande)
      throws TechnicalErrorException;

  Optional<SupplierOrder> getSupplierOrderById(Long id) throws TechnicalErrorException;

  Optional<SupplierOrder> getSupplierOrderByCode(String code) throws TechnicalErrorException;

  List<SupplierOrder> getAllSupplierOrder();

  void deleteSupplierOrder(Long id);
}
