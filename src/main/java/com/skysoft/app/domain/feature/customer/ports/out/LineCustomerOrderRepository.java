package com.skysoft.app.domain.feature.customer.ports.out;

import com.skysoft.app.domain.model.LineCustomerOrder;
import java.util.List;
import java.util.Optional;

public interface LineCustomerOrderRepository {
  List<LineCustomerOrder> findAllLignesCommandesClientByCommandeClientId(Long idCommande);

  List<LineCustomerOrder> findHistoriaueCommandeClient(Long productId);

  LineCustomerOrder saveLineCustomerOrder(LineCustomerOrder lineCustomerOrder);

  void deleteLineCustomerOrder(Long lineCustomerOrderId);

  Optional<LineCustomerOrder> findByLineCustomerOrderId(Long idLigneCommande);
}
