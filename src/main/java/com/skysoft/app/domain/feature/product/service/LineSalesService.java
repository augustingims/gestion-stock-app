package com.skysoft.app.domain.feature.product.service;

import com.skysoft.app.domain.feature.product.ports.in.LineSalesUseCases;
import com.skysoft.app.domain.feature.product.ports.out.LineSalesRepository;
import com.skysoft.app.domain.model.LineSales;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class LineSalesService implements LineSalesUseCases {
  private final LineSalesRepository lineSalesRepository;

  @Override
  public List<LineSales> findHistoriqueVentes(Long productId) {
    return lineSalesRepository.findHistoriqueVentes(productId);
  }
}
