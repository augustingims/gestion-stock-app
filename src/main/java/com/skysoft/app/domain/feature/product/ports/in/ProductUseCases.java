package com.skysoft.app.domain.feature.product.ports.in;

import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.model.*;
import java.util.List;
import java.util.Optional;

public interface ProductUseCases {
  Product createArticle(Product product);

  Optional<Product> getArticleById(Long productId) throws TechnicalErrorException;

  Optional<Product> getByCodeArticle(String codeProduct) throws TechnicalErrorException;

  List<Product> getAllArticleByIdCategory(Long categoryId);

  List<Product> getAllProducts(final Long id, final String code);

  void deleteArticle(Long productId);

  Optional<Product> updateProduct(Product product);
}
