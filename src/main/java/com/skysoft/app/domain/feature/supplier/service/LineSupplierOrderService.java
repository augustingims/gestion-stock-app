package com.skysoft.app.domain.feature.supplier.service;

import com.skysoft.app.domain.feature.supplier.ports.in.LineSupplierOrderUseCases;
import com.skysoft.app.domain.feature.supplier.ports.out.LineSupplierOrderRepository;
import com.skysoft.app.domain.model.LineSupplierOrder;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class LineSupplierOrderService implements LineSupplierOrderUseCases {
  private final LineSupplierOrderRepository lineSupplierOrderRepository;

  @Override
  public List<LineSupplierOrder> getAllLignesCommandesFournisseurByCommandeFournisseurId(
      Long idCommande) {
    return lineSupplierOrderRepository.findAllLignesCommandesFournisseurByCommandeFournisseurId(
        idCommande);
  }

  @Override
  public List<LineSupplierOrder> getHistoriqueCommandeFournisseur(Long productId) {
    return lineSupplierOrderRepository.findHistoriqueCommandeFournisseur(productId);
  }
}
