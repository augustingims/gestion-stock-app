package com.skysoft.app.domain.feature.supplier.ports.out;

import com.skysoft.app.domain.model.SupplierOrder;
import java.util.List;
import java.util.Optional;

public interface SupplierOrderRepository {
  SupplierOrder saveSupplierOrder(SupplierOrder supplierOrder);

  Optional<SupplierOrder> findSupplierOrderById(Long id);

  List<SupplierOrder> findAllSupplierOrderBySupplierId(Long supplierId);

  Optional<SupplierOrder> findSupplierOrderByCode(String code);

  List<SupplierOrder> findAllSupplierOrder();

  void deleteSupplierOrder(Long id);
}
