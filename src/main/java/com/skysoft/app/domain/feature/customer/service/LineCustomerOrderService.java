package com.skysoft.app.domain.feature.customer.service;

import com.skysoft.app.domain.feature.customer.ports.in.LineCustomerOrderUseCases;
import com.skysoft.app.domain.feature.customer.ports.out.LineCustomerOrderRepository;
import com.skysoft.app.domain.model.LineCustomerOrder;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class LineCustomerOrderService implements LineCustomerOrderUseCases {
  private final LineCustomerOrderRepository lineCustomerOrderRepository;

  @Override
  public List<LineCustomerOrder> getAllLignesCommandesClientByCommandeClientId(Long idCommande) {
    return lineCustomerOrderRepository.findAllLignesCommandesClientByCommandeClientId(idCommande);
  }

  @Override
  public List<LineCustomerOrder> getHistoriaueCommandeClient(Long productId) {
    return lineCustomerOrderRepository.findHistoriaueCommandeClient(productId);
  }
}
