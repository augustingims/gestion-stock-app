package com.skysoft.app.domain.feature.product.ports.out;

import com.skysoft.app.domain.model.*;
import java.util.List;

public interface LineSalesRepository {

  List<LineSales> findHistoriqueVentes(Long productId);

  LineSales saveVente(LineSales lineSales);

  List<LineSales> findAllByVenteId(Long venteId);
}
