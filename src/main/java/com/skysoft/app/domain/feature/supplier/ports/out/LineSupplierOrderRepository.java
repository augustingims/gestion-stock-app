package com.skysoft.app.domain.feature.supplier.ports.out;

import com.skysoft.app.domain.model.LineSupplierOrder;
import java.util.List;
import java.util.Optional;

public interface LineSupplierOrderRepository {
  List<LineSupplierOrder> findAllLignesCommandesFournisseurByCommandeFournisseurId(Long idCommande);

  List<LineSupplierOrder> findHistoriqueCommandeFournisseur(Long productId);

  LineSupplierOrder saveLineSupplierOrder(LineSupplierOrder lineSupplierOrder);

  void deleteLineSupplierOrder(Long lineSupplierOrderId);

  Optional<LineSupplierOrder> findByLineSupplierOrderId(Long lineSupplierOrderId);
}
