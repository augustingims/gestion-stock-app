package com.skysoft.app.domain.feature.supplier.ports.in;

import com.skysoft.app.domain.model.LineSupplierOrder;
import java.util.List;

public interface LineSupplierOrderUseCases {
  List<LineSupplierOrder> getAllLignesCommandesFournisseurByCommandeFournisseurId(Long idCommande);

  List<LineSupplierOrder> getHistoriqueCommandeFournisseur(Long productId);
}
