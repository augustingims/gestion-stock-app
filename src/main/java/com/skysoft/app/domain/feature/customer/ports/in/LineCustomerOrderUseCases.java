package com.skysoft.app.domain.feature.customer.ports.in;

import com.skysoft.app.domain.model.LineCustomerOrder;
import java.util.List;

public interface LineCustomerOrderUseCases {
  List<LineCustomerOrder> getAllLignesCommandesClientByCommandeClientId(Long idCommande);

  List<LineCustomerOrder> getHistoriaueCommandeClient(Long productId);
}
