package com.skysoft.app.domain.feature.product.ports.out;

import com.skysoft.app.domain.model.*;
import java.util.List;
import java.util.Optional;

public interface SalesRepository {
  Sales saveSales(Sales sales);

  Optional<Sales> findSalesById(Long id);

  Optional<Sales> findSalesByCode(String code);

  List<Sales> findAllSales();

  void deleteSales(Long id);
}
