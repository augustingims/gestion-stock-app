package com.skysoft.app.domain.feature.product.service;

import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.feature.product.ports.in.InventoryMovementUseCases;
import com.skysoft.app.domain.feature.product.ports.out.InventoryMovementRepository;
import com.skysoft.app.domain.model.InventoryMovement;
import com.skysoft.app.domain.model.enumeration.TypeInventoryMovement;
import com.skysoft.app.domain.validator.InventoryMovementValidator;
import java.math.BigDecimal;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class InventoryMovementService implements InventoryMovementUseCases {
  private final InventoryMovementRepository inventoryMovementRepository;

  @Override
  public BigDecimal stockReelArticle(Long productId) {
    if (productId == null) {
      log.warn("ID product is NULL");
      return BigDecimal.valueOf(-1);
    }
    return inventoryMovementRepository.stockReelArticle(productId);
  }

  @Override
  public List<InventoryMovement> mvtStkArticle(Long productId) {
    return inventoryMovementRepository.mvtStkArticle(productId);
  }

  @Override
  public InventoryMovement entreeStock(InventoryMovement inventoryMovement) {
    return entreePositive(inventoryMovement, TypeInventoryMovement.ENTREE);
  }

  @Override
  public InventoryMovement sortieStock(InventoryMovement inventoryMovement) {
    return sortieNegative(inventoryMovement, TypeInventoryMovement.SORTIE);
  }

  @Override
  public InventoryMovement correctionStockPos(InventoryMovement inventoryMovement) {
    return entreePositive(inventoryMovement, TypeInventoryMovement.CORRECTION_POS);
  }

  @Override
  public InventoryMovement correctionStockNeg(InventoryMovement inventoryMovement) {
    return sortieNegative(inventoryMovement, TypeInventoryMovement.CORRECTION_NEG);
  }

  private InventoryMovement entreePositive(
      InventoryMovement inventoryMovement, TypeInventoryMovement typeMvtStk) {
    List<String> errors = InventoryMovementValidator.validate(inventoryMovement);
    if (!errors.isEmpty()) {
      log.error("Inventory Movement is not valid {}", inventoryMovement);
      throw new FunctionalErrorException(
          "Le mouvement du stock n'est pas valide", ErrorCodes.MVT_STK_NOT_VALID, errors);
    }
    inventoryMovement.setQuantite(
        BigDecimal.valueOf(Math.abs(inventoryMovement.getQuantite().doubleValue())));
    inventoryMovement.setTypeMvtStk(typeMvtStk);
    return inventoryMovementRepository.saveInventoryMovement(inventoryMovement);
  }

  private InventoryMovement sortieNegative(
      InventoryMovement inventoryMovement, TypeInventoryMovement typeMvtStk) {
    List<String> errors = InventoryMovementValidator.validate(inventoryMovement);
    if (!errors.isEmpty()) {
      log.error("Inventory Movement is not valid {}", inventoryMovement);
      throw new FunctionalErrorException(
          "Le mouvement du stock n'est pas valide", ErrorCodes.MVT_STK_NOT_VALID, errors);
    }
    inventoryMovement.setQuantite(
        BigDecimal.valueOf(Math.abs(inventoryMovement.getQuantite().doubleValue()) * -1));
    inventoryMovement.setTypeMvtStk(typeMvtStk);
    return inventoryMovementRepository.saveInventoryMovement(inventoryMovement);
  }
}
