package com.skysoft.app.domain.feature.product.ports.in;

import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.model.*;
import java.util.List;
import java.util.Optional;

public interface CategoryUseCases {

  Category createCategory(Category category);

  Optional<Category> getCategoryById(Long categoryId) throws TechnicalErrorException;

  Optional<Category> getCategoryByCode(String code) throws TechnicalErrorException;

  List<Category> getAllCategory();

  void deleteCategory(Long categoryId);
}
