package com.skysoft.app.domain.feature.customer.service;

import com.skysoft.app.domain.model.Customer;
import com.skysoft.app.domain.service.PictureStrategy;
import java.io.InputStream;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service("customerPictureStrategyService")
@Slf4j
@RequiredArgsConstructor
public class CustomerPictureStrategyService implements PictureStrategy<Customer> {
  @Override
  public Optional<Customer> upload(final Long id, final InputStream photo, final String titre) {
    return Optional.empty();
  }
}
