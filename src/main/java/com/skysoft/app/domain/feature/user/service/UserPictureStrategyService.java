package com.skysoft.app.domain.feature.user.service;

import com.skysoft.app.domain.model.User;
import com.skysoft.app.domain.service.PictureStrategy;
import java.io.InputStream;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service("userPictureStrategyService")
@Slf4j
@RequiredArgsConstructor
public class UserPictureStrategyService implements PictureStrategy<User> {
  @Override
  public Optional<User> upload(final Long id, final InputStream photo, final String titre) {
    return Optional.empty();
  }
}
