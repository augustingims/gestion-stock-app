package com.skysoft.app.domain.feature.company.service;

import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.feature.company.ports.in.CompanyUseCases;
import com.skysoft.app.domain.feature.company.ports.out.CompanyRepository;
import com.skysoft.app.domain.feature.user.ports.out.UserRepository;
import com.skysoft.app.domain.model.Authority;
import com.skysoft.app.domain.model.Company;
import com.skysoft.app.domain.model.User;
import com.skysoft.app.domain.validator.CompanyValidator;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class CompanyService implements CompanyUseCases {

  private final CompanyRepository companyRepository;
  private final UserRepository userRepository;

  @Override
  @Transactional(rollbackFor = {FunctionalErrorException.class})
  public Company createCompany(final Company company, final String password) {
    final List<String> errors = CompanyValidator.validate(company);
    if (!errors.isEmpty()) {
      log.error("company is not valid {}", company);
      throw new FunctionalErrorException(
          "L'entreprise n'est pas valide", ErrorCodes.ENTREPRISE_NOT_VALID, errors);
    }
    final Company companySave = companyRepository.saveCompany(company);
    final User userCompany = userRepository.saveUser(buildUser(companySave, password));

    final Authority authority = Authority.builder().roleName("ADMIN").utilisateur(userCompany).build();
    userRepository.saveAuthority(authority);

    return companySave;
  }

  private User buildUser(final Company companySave, final String password) {
    return User.builder()
        .nom(companySave.getNom())
        .email(companySave.getEmail())
        .motDePasse(password)
        .entreprise(companySave)
        .dateNaissance(Instant.now())
        .photo(companySave.getPhoto())
        .build();
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Company> getCompany(final Long companyId) {
    if (companyId == null) {
      log.error("Entreprise ID is null");
      return Optional.empty();
    }
    return companyRepository.findCompany(companyId);
  }

  @Override
  public List<Company> getAllCompany() {
    return companyRepository.findAllCompany();
  }

  @Override
  public Optional<Company> updateCompany(final Company company) {
    return Optional.ofNullable(companyRepository.saveCompany(company));
  }
}
