package com.skysoft.app.domain.feature.customer.service;

import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.customer.ports.in.CustomerUseCases;
import com.skysoft.app.domain.feature.customer.ports.out.CustomerOrderRepository;
import com.skysoft.app.domain.feature.customer.ports.out.CustomerRepository;
import com.skysoft.app.domain.model.Customer;
import com.skysoft.app.domain.model.CustomerOrder;
import com.skysoft.app.domain.utils.MessageUtils;
import com.skysoft.app.domain.validator.CustomerValidator;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CustomerService implements CustomerUseCases {
  private final CustomerOrderRepository customerOrderRepository;
  private final CustomerRepository customerRepository;

  @Override
  public Customer createCustomer(Customer customer) {
    List<String> errors = CustomerValidator.validate(customer);
    if (!errors.isEmpty()) {
      log.error("Le client n'est pas valide {}", customer);
      throw new FunctionalErrorException(
          "Le client n'est pas valide", ErrorCodes.CLIENT_NOT_VALID, errors);
    }
    return customerRepository.saveCustomer(customer);
  }

  @Override
  public Optional<Customer> getCustomerById(Long id) throws TechnicalErrorException {
    if (null == id) {
      log.error("l'id du client ne doit etre null");
      return Optional.empty();
    }
    return Optional.ofNullable(
        customerRepository
            .findCustomerById(id)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        String.format(MessageUtils.MESSAGE_M, "client", "l'ID", id),
                        ErrorCodes.CLIENT_NOT_FOUND)));
  }

  @Override
  public List<Customer> getAllCustomer() {
    return customerRepository.findAllCustomer();
  }

  @Override
  public void deleteCustomer(Long id) {
    if (null == id) {
      log.error("l'id du client ne doit pas etre null");
      return;
    }
    List<CustomerOrder> commandeClients = customerOrderRepository.findAllByCustomerId(id);
    if (!commandeClients.isEmpty()) {
      throw new FunctionalErrorException(
          "Impossible de supprimer un client qui a deja des commandes",
          ErrorCodes.CLIENT_ALREADY_IN_USE);
    }
    customerRepository.deleteCustomer(id);
  }
}
