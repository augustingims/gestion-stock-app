package com.skysoft.app.domain.feature.customer.ports.out;

import com.skysoft.app.domain.model.CustomerOrder;
import java.util.List;
import java.util.Optional;

public interface CustomerOrderRepository {
  CustomerOrder saveCustomerOrder(CustomerOrder customerOrder);

  Optional<CustomerOrder> findCustomerOrderById(Long id);

  Optional<CustomerOrder> findCustomerOrderByCode(String code);

  List<CustomerOrder> findAllCustomerOrder();

  List<CustomerOrder> findAllByCustomerId(Long customerId);

  void deleteCustomerOrder(Long id);
}
