package com.skysoft.app.domain.feature.user.ports.in;

import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.model.User;
import java.util.List;
import java.util.Optional;

public interface UserUseCases {
  User createUser(User user);

  Optional<User> getById(Long userId) throws TechnicalErrorException;

  Optional<User> getByEmail(String email) throws TechnicalErrorException;

  User changePassword(Long id, String password, String confirmPassword);

  Optional<User> getCurrentUser();

  List<User> getAllUsers(Long id, String email);
}
