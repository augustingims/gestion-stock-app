package com.skysoft.app.domain.feature.product.ports.in;

import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.model.*;
import java.util.List;
import java.util.Optional;

public interface SalesUseCases {
  Sales createSales(Sales sales);

  Optional<Sales> getSalesById(Long id) throws TechnicalErrorException;

  Optional<Sales> getSalesByCode(String code) throws TechnicalErrorException;

  List<Sales> getAllSales();

  void deleteSales(Long id);
}
