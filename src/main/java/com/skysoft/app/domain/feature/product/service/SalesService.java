package com.skysoft.app.domain.feature.product.service;

import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.product.ports.in.SalesUseCases;
import com.skysoft.app.domain.feature.product.ports.out.InventoryMovementRepository;
import com.skysoft.app.domain.feature.product.ports.out.LineSalesRepository;
import com.skysoft.app.domain.feature.product.ports.out.ProductRepository;
import com.skysoft.app.domain.feature.product.ports.out.SalesRepository;
import com.skysoft.app.domain.model.InventoryMovement;
import com.skysoft.app.domain.model.LineSales;
import com.skysoft.app.domain.model.Product;
import com.skysoft.app.domain.model.Sales;
import com.skysoft.app.domain.model.enumeration.SourceInventoryMovement;
import com.skysoft.app.domain.model.enumeration.TypeInventoryMovement;
import com.skysoft.app.domain.utils.MessageUtils;
import com.skysoft.app.domain.validator.SalesValidator;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class SalesService implements SalesUseCases {
  private final SalesRepository salesRepository;
  private final LineSalesRepository lineSalesRepository;
  private final InventoryMovementRepository inventoryMovementRepository;
  private final ProductRepository productRepository;

  @Override
  public Sales createSales(Sales sales) {
    List<String> errors = SalesValidator.validate(sales);
    if (!errors.isEmpty()) {
      log.error("Vente n'est pas valide {}", sales);
      throw new FunctionalErrorException(
          "La vente n'est pas valide", ErrorCodes.VENTE_NOT_VALID, errors);
    }
    List<String> articleErrors = new ArrayList<>();
    sales
        .getLigneVentes()
        .forEach(
            ligneVenteDto -> {
              Optional<Product> product =
                  productRepository.findArticleById(ligneVenteDto.getArticle().getId());
              if (product.isEmpty()) {
                articleErrors.add(
                    String.format(
                        MessageUtils.MESSAGE_M,
                        "article",
                        "l'ID",
                        ligneVenteDto.getArticle().getId().toString()));
              }
            });

    if (!articleErrors.isEmpty()) {
      log.error("Un ou plusieurs article(s) n'ont pas ete trouve dans la BD, {}", articleErrors);
      throw new FunctionalErrorException(
          "Un ou plusieurs article(s) n'ont pas ete trouve dans la BD",
          ErrorCodes.VENTE_NOT_VALID,
          articleErrors);
    }

    Sales savedVente = salesRepository.saveSales(sales);

    sales
        .getLigneVentes()
        .forEach(
            ligneVente -> {
              ligneVente.setVente(savedVente);
              lineSalesRepository.saveVente(ligneVente);
              updateMvtStk(ligneVente);
            });

    return savedVente;
  }

  private void updateMvtStk(LineSales lineSales) {
    InventoryMovement mvtStkDto =
        InventoryMovement.builder()
            .article(lineSales.getArticle())
            .dateMvt(Instant.now())
            .typeMvtStk(TypeInventoryMovement.SORTIE)
            .sourceMvtStk(SourceInventoryMovement.VENTE)
            .quantite(lineSales.getQuantite())
            .idEntreprise(lineSales.getIdEntreprise())
            .build();
    inventoryMovementRepository.saveInventoryMovement(mvtStkDto);
  }

  @Override
  public Optional<Sales> getSalesById(Long id) throws TechnicalErrorException {
    if (null == id) {
      log.error("L'id vente ne doit pas etre null");
      return Optional.empty();
    }
    return Optional.ofNullable(
        salesRepository
            .findSalesById(id)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        String.format(MessageUtils.MESSAGE_F, "vente", "l'ID", id),
                        ErrorCodes.VENTE_NOT_FOUND)));
  }

  @Override
  public Optional<Sales> getSalesByCode(String code) throws TechnicalErrorException {
    if (null == code) {
      log.error("Le code vente ne doit pas etre null");
      return Optional.empty();
    }
    return Optional.ofNullable(
        salesRepository
            .findSalesByCode(code)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        String.format(MessageUtils.MESSAGE_F, "vente client", "le Code", code),
                        ErrorCodes.VENTE_NOT_VALID)));
  }

  @Override
  public List<Sales> getAllSales() {
    return salesRepository.findAllSales();
  }

  @Override
  public void deleteSales(Long id) {
    if (null == id) {
      log.error("L'id vente ne doit pas etre null");
      return;
    }
    List<LineSales> ligneVentes = lineSalesRepository.findAllByVenteId(id);
    if (!ligneVentes.isEmpty()) {
      throw new FunctionalErrorException(
          "Impossible de supprimer une vente...", ErrorCodes.CLIENT_ALREADY_IN_USE);
    }
    salesRepository.deleteSales(id);
  }
}
