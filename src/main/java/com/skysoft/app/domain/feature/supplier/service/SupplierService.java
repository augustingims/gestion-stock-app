package com.skysoft.app.domain.feature.supplier.service;

import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.FunctionalErrorException;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.domain.feature.supplier.ports.in.SupplierUseCases;
import com.skysoft.app.domain.feature.supplier.ports.out.SupplierOrderRepository;
import com.skysoft.app.domain.feature.supplier.ports.out.SupplierRepository;
import com.skysoft.app.domain.model.Supplier;
import com.skysoft.app.domain.model.SupplierOrder;
import com.skysoft.app.domain.utils.MessageUtils;
import com.skysoft.app.domain.validator.SupplierValidator;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class SupplierService implements SupplierUseCases {
  private final SupplierRepository supplierRepository;
  private final SupplierOrderRepository supplierOrderRepository;

  @Override
  public Supplier createSupplier(Supplier supplier) {
    List<String> errors = SupplierValidator.validate(supplier);
    if (!errors.isEmpty()) {
      log.error("Le Fournisseur est invalide {}", supplier);
      throw new FunctionalErrorException(
          "Le Fournisseur est invalide", ErrorCodes.FOURNISSEUR_NOT_VALID, errors);
    }
    return supplierRepository.saveSupplier(supplier);
  }

  @Override
  public Optional<Supplier> getSupplierById(Long supplierId) throws TechnicalErrorException {
    if (null == supplierId) {
      log.error("l'id du fournisseur ne doit pas etre null");
      return Optional.empty();
    }
    return Optional.ofNullable(
        supplierRepository
            .findSupplierById(supplierId)
            .orElseThrow(
                () ->
                    new TechnicalErrorException(
                        String.format(MessageUtils.MESSAGE_M, "fournisseur", "l'ID", supplierId),
                        ErrorCodes.FOURNISSEUR_NOT_FOUND)));
  }

  @Override
  public List<Supplier> getAllSupplier() {
    return supplierRepository.findAllSupplier();
  }

  @Override
  public void deleteSupplier(Long supplierId) {
    if (null == supplierId) {
      log.error("l'id du fournisseur ne doit pas etre null");
      return;
    }
    List<SupplierOrder> list = supplierOrderRepository.findAllSupplierOrderBySupplierId(supplierId);
    if (!list.isEmpty()) {
      throw new FunctionalErrorException(
          "Impossible de supprimer un fournisseur qui a deja des commandes",
          ErrorCodes.COMMANDE_FOURNISSEUR_ALREADY_IN_USE);
    }
    supplierRepository.deleteSupplier(supplierId);
  }
}
