package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.AdresseDto;
import com.skysoft.app.application.dto.RolesDto;
import com.skysoft.app.application.dto.UtilisateurDto;
import com.skysoft.app.domain.model.Address;
import com.skysoft.app.domain.model.Authority;
import com.skysoft.app.domain.model.User;
import com.skysoft.app.infrastructure.connector.db.entity.Adresse;
import com.skysoft.app.infrastructure.connector.db.entity.Roles;
import com.skysoft.app.infrastructure.connector.db.entity.Utilisateur;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UtilisateurMapper {
  UtilisateurMapper INSTANCE = Mappers.getMapper(UtilisateurMapper.class);

  @Mapping(target = "adresse", source = "adresse", qualifiedByName = "domainAdresseId")
  @Mapping(target = "roles", source = "roles", qualifiedByName = "domainRoleId")
  User toDomain(Utilisateur utilisateur);

  @Mapping(target = "adresse", source = "adresse", qualifiedByName = "domainAdresseIdDto")
  @Mapping(target = "roles", source = "roles", qualifiedByName = "domainRoleIdDto")
  User toDomain(UtilisateurDto utilisateurDto);

  @Mapping(target = "adresse", source = "adresse", qualifiedByName = "dtoAdresseId")
  @Mapping(target = "roles", source = "roles", qualifiedByName = "dtoRoleId")
  UtilisateurDto toDto(User user);

  @Mapping(target = "adresse", source = "adresse", qualifiedByName = "entityAdresseId")
  @Mapping(target = "roles", source = "roles", qualifiedByName = "entityRoleId")
  Utilisateur toEntity(User user);

  @Mapping(target = "adresse", source = "adresse", qualifiedByName = "entityAdresseIdDto")
  @Mapping(target = "roles", source = "roles", qualifiedByName = "entityRoleIdDto")
  Utilisateur toEntity(UtilisateurDto utilisateurDto);

  @Named("domainAdresseId")
  Address toDomainAdresseId(Adresse adresse);

  @Named("domainAdresseIdDto")
  Address toDomainAdresseIdDto(AdresseDto adresseDto);

  @Named("dtoAdresseId")
  AdresseDto toDtoAdresseId(Address address);

  @Named("entityAdresseIdDto")
  Adresse toEntityAdresseIdDto(AdresseDto adresseDto);

  @Named("entityAdresseId")
  Adresse toEntityAdresseId(Address address);

  @Named("domainRoleId")
  @BeanMapping(ignoreByDefault = true)
  @Mapping(target = "roleName", source = "roleName")
  Authority toDomainRoleId(Roles roles);

  @Named("domainRoleIdDto")
  @BeanMapping(ignoreByDefault = true)
  @Mapping(target = "roleName", source = "roleName")
  Authority toDomainRoleIdDto(RolesDto rolesDto);

  @Named("dtoRoleId")
  @BeanMapping(ignoreByDefault = true)
  @Mapping(target = "roleName", source = "roleName")
  RolesDto toDto(Authority authority);

  @Named("entityRoleId")
  @BeanMapping(ignoreByDefault = true)
  @Mapping(target = "roleName", source = "roleName")
  Roles toEntityRoleId(Authority authority);

  @Named("entityRoleIdDto")
  @BeanMapping(ignoreByDefault = true)
  @Mapping(target = "roleName", source = "roleName")
  Roles toEntityRoleIdDto(RolesDto rolesDto);
}
