package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.LigneCommandeClientDto;
import com.skysoft.app.domain.model.LineCustomerOrder;
import com.skysoft.app.infrastructure.connector.db.entity.LigneCommandeClient;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface LigneCommandeClientMapper {
  LigneCommandeClientMapper INSTANCE = Mappers.getMapper(LigneCommandeClientMapper.class);

  LineCustomerOrder toDomain(LigneCommandeClient ligneCommandeClient);

  LineCustomerOrder toDomain(LigneCommandeClientDto ligneCommandeClientDto);

  LigneCommandeClientDto toDto(LineCustomerOrder lineCustomerOrder);

  LigneCommandeClient toEntity(LineCustomerOrder lineCustomerOrder);

  LigneCommandeClient toEntity(LigneCommandeClientDto ligneCommandeClientDto);
}
