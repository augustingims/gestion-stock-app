package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.RolesDto;
import com.skysoft.app.domain.model.Authority;
import com.skysoft.app.infrastructure.connector.db.entity.Roles;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(
    componentModel = "spring")
public interface RolesMapper {
  RolesMapper INSTANCE = Mappers.getMapper(RolesMapper.class);

  Authority toDomain(Roles roles);

  Authority toDomain(RolesDto rolesDto);

  RolesDto toDto(Authority authority);

  Roles toEntity(Authority authority);

  Roles toEntity(RolesDto rolesDto);
}
