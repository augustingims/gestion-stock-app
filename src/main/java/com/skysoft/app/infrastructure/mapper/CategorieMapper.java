package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.CategorieDto;
import com.skysoft.app.domain.model.Category;
import com.skysoft.app.infrastructure.connector.db.entity.Categorie;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CategorieMapper {
  CategorieMapper INSTANCE = Mappers.getMapper(CategorieMapper.class);

  Category toDomain(CategorieDto categorieDto);

  Category toDomain(Categorie categorie);

  CategorieDto toDto(Category category);

  Categorie toEntity(Category category);

  Categorie toEntity(CategorieDto categorieDto);
}
