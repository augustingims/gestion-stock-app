package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.AdresseDto;
import com.skysoft.app.application.dto.EntrepriseDto;
import com.skysoft.app.application.dto.RolesDto;
import com.skysoft.app.domain.model.Address;
import com.skysoft.app.domain.model.Authority;
import com.skysoft.app.domain.model.Company;
import com.skysoft.app.infrastructure.connector.db.entity.Adresse;
import com.skysoft.app.infrastructure.connector.db.entity.Entreprise;
import com.skysoft.app.infrastructure.connector.db.entity.Roles;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(
    componentModel = "spring")
public interface EntrepriseMapper {

  EntrepriseMapper INSTANCE = Mappers.getMapper(EntrepriseMapper.class);

  @Mapping(target = "adresse", source = "adresse", qualifiedByName = "domainAdresseId")
  Company toDomain(Entreprise entreprise);

  @Mapping(target = "adresse", source = "adresse", qualifiedByName = "domainAdresseIdDto")
  Company toDomain(EntrepriseDto entrepriseDto);

  @Mapping(target = "adresse", source = "adresse", qualifiedByName = "dtoAdresseId")
  EntrepriseDto toDto(Company company);

  @Mapping(target = "adresse", source = "adresse", qualifiedByName = "entityAdresseId")
  Entreprise toEntity(Company company);

  @Mapping(target = "adresse", source = "adresse", qualifiedByName = "entityAdresseIdDto")
  Entreprise toEntity(EntrepriseDto entrepriseDto);

  @Named("domainAdresseId")
  Address toDomainAdresseId(Adresse adresse);
  @Named("domainAdresseIdDto")
  Address toDomainAdresseIdDto(AdresseDto adresseDto);
  @Named("dtoAdresseId")
  AdresseDto toDtoAdresseId(Address address);
  @Named("entityAdresseIdDto")
  Adresse toEntityAdresseIdDto(AdresseDto adresseDto);
  @Named("entityAdresseId")
  Adresse toEntityAdresseId(Address address);

}
