package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.CommandeClientDto;
import com.skysoft.app.domain.model.CustomerOrder;
import com.skysoft.app.infrastructure.connector.db.entity.CommandeClient;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CommandeClientMapper {
  CommandeClientMapper INSTANCE = Mappers.getMapper(CommandeClientMapper.class);

  CustomerOrder toDomain(CommandeClient commandeClient);

  CustomerOrder toDomain(CommandeClientDto commandeClientDto);

  CommandeClientDto toDto(CustomerOrder customerOrder);

  CommandeClient toEntity(CustomerOrder customerOrder);

  CommandeClient toEntity(CommandeClientDto commandeClientDto);
}
