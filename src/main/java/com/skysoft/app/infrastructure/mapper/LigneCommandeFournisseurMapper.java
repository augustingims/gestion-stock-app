package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.LigneCommandeFournisseurDto;
import com.skysoft.app.domain.model.LineSupplierOrder;
import com.skysoft.app.infrastructure.connector.db.entity.LigneCommandeFournisseur;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface LigneCommandeFournisseurMapper {
  LigneCommandeFournisseurMapper INSTANCE = Mappers.getMapper(LigneCommandeFournisseurMapper.class);

  LineSupplierOrder toDomain(LigneCommandeFournisseur ligneCommandeFournisseur);

  LineSupplierOrder toDomain(LigneCommandeFournisseurDto ligneCommandeFournisseurDto);

  LigneCommandeFournisseurDto toDto(LineSupplierOrder lineSupplierOrder);

  LigneCommandeFournisseur toEntity(LineSupplierOrder lineSupplierOrder);

  LigneCommandeFournisseur toEntity(LigneCommandeFournisseurDto commandeFournisseurDto);
}
