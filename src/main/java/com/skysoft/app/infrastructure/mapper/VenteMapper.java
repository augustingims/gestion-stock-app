package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.VenteDto;
import com.skysoft.app.domain.model.Sales;
import com.skysoft.app.infrastructure.connector.db.entity.Vente;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface VenteMapper {
  VenteMapper INSTANCE = Mappers.getMapper(VenteMapper.class);

  Sales toDomain(Vente vente);

  Sales toDomain(VenteDto venteDto);

  VenteDto toDto(Sales sales);

  Vente toEntity(Sales sales);

  Vente toEntity(VenteDto venteDto);
}
