package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.LigneVenteDto;
import com.skysoft.app.domain.model.LineSales;
import com.skysoft.app.infrastructure.connector.db.entity.LigneVente;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface LigneVenteMapper {
  LigneVenteMapper INSTANCE = Mappers.getMapper(LigneVenteMapper.class);

  LineSales toDomain(LigneVente vente);

  LineSales toDomain(LigneVenteDto venteDto);

  LigneVenteDto toDto(LineSales lineSales);

  LigneVente toEntity(LineSales lineSales);

  LigneVente toEntity(LigneVenteDto ligneVenteDto);
}
