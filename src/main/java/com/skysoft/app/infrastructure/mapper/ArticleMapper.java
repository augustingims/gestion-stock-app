package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.ArticleDto;
import com.skysoft.app.application.dto.CategorieDto;
import com.skysoft.app.domain.model.Category;
import com.skysoft.app.domain.model.Product;
import com.skysoft.app.infrastructure.connector.db.entity.Article;
import com.skysoft.app.infrastructure.connector.db.entity.Categorie;
import java.util.List;
import java.util.Set;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ArticleMapper {
  ArticleMapper INSTANCE = Mappers.getMapper(ArticleMapper.class);

  Set<Article> toSetDomain(List<Product> list);

  @Mapping(target = "categorie", source = "categorie", qualifiedByName = "domainCategorieIdDto")
  Product toDomain(ArticleDto articleDto);

  @Mapping(target = "categorie", source = "categorie", qualifiedByName = "domainCategorieId")
  Product toDomain(Article article);

  @Mapping(target = "categorie", source = "categorie", qualifiedByName = "dtoCategorieId")
  ArticleDto toDto(Product product);

  @Mapping(target = "categorie", source = "categorie", qualifiedByName = "entityCategorieId")
  Article toEntity(Product product);

  @Mapping(target = "categorie", source = "categorie", qualifiedByName = "entityCategorieIdDto")
  Article toEntity(ArticleDto articleDto);

  @Named("domainCategorieId")
  Category toDomainCategorieId(Categorie categorie);

  @Named("domainCategorieIdDto")
  Category toDomainCategorieIdDto(CategorieDto categorieDto);

  @Named("dtoCategorieId")
  CategorieDto toDto(Category category);

  @Named("entityCategorieId")
  Categorie toEntityCategorieId(Category category);

  @Named("entityCategorieIdDto")
  Categorie toEntityCategorieIdDto(CategorieDto categorieDto);
}
