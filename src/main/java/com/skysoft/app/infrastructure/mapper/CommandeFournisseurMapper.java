package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.CommandeFournisseurDto;
import com.skysoft.app.domain.model.SupplierOrder;
import com.skysoft.app.infrastructure.connector.db.entity.CommandeFournisseur;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CommandeFournisseurMapper {
  CommandeFournisseurMapper INSTANCE = Mappers.getMapper(CommandeFournisseurMapper.class);

  SupplierOrder toDomain(CommandeFournisseurDto commandeFournisseurDto);

  SupplierOrder toDomain(CommandeFournisseur commandeFournisseur);

  CommandeFournisseurDto toDto(SupplierOrder supplierOrder);

  CommandeFournisseur toEntity(SupplierOrder supplierOrder);

  CommandeFournisseur toEntity(CommandeFournisseurDto commandeFournisseurDto);
}
