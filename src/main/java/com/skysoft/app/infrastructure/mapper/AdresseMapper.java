package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.AdresseDto;
import com.skysoft.app.domain.model.Address;
import com.skysoft.app.infrastructure.connector.db.entity.Adresse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(
    componentModel = "spring")
public interface AdresseMapper {

  AdresseMapper INSTANCE = Mappers.getMapper(AdresseMapper.class);

  Address toDomain(Adresse adresse);
  Address toDomain(AdresseDto adresseDto);

  AdresseDto toDto(Address address);

  Adresse toEntity(Address address);
  Adresse toEntity(AdresseDto adresseDto);
}
