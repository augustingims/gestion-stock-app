package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.ClientDto;
import com.skysoft.app.domain.model.Customer;
import com.skysoft.app.infrastructure.connector.db.entity.Client;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ClientMapper {
  ClientMapper INSTANCE = Mappers.getMapper(ClientMapper.class);

  Customer toDomain(ClientDto clientDto);

  Customer toDomain(Client client);

  ClientDto toDto(Customer customer);

  Client toEntity(Customer customer);

  Client toEntity(ClientDto clientDto);
}
