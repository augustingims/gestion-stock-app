package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.FournisseurDto;
import com.skysoft.app.domain.model.Supplier;
import com.skysoft.app.infrastructure.connector.db.entity.Fournisseur;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface FournisseurMapper {
  FournisseurMapper INSTANCE = Mappers.getMapper(FournisseurMapper.class);

  Supplier toDomain(Fournisseur fournisseur);

  Supplier toDomain(FournisseurDto fournisseurDto);

  FournisseurDto toDto(Supplier supplier);

  Fournisseur toEntity(Supplier supplier);

  Fournisseur toEntity(FournisseurDto fournisseurDto);
}
