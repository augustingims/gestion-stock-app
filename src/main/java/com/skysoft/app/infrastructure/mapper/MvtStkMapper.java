package com.skysoft.app.infrastructure.mapper;

import com.skysoft.app.application.dto.MvtStkDto;
import com.skysoft.app.domain.model.InventoryMovement;
import com.skysoft.app.infrastructure.connector.db.entity.MvtStk;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface MvtStkMapper {
  MvtStkMapper INSTANCE = Mappers.getMapper(MvtStkMapper.class);

  InventoryMovement toDomain(MvtStk mvtStk);

  InventoryMovement toDomain(MvtStkDto mvtStkDto);

  MvtStkDto toDto(InventoryMovement inventoryMovement);

  MvtStk toEntity(InventoryMovement inventoryMovement);

  MvtStk toEntity(MvtStkDto mvtStkDto);
}
