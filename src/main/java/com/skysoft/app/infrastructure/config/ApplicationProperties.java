package com.skysoft.app.infrastructure.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

  private final TaskAsync taskAsync = new TaskAsync();
  private final Flickr flickr = new Flickr();
  private final Jwt jwt = new Jwt();

  public TaskAsync getTaskAsync() {
    return taskAsync;
  }

  public Flickr getFlickr() {
    return flickr;
  }

  public Jwt getJwt() {
    return jwt;
  }

  @Getter
  @Setter
  public static class TaskAsync {
    private int corePoolSize;
    private int maxPoolSize;
    private int queueCapacity;
    private String threadNamePrefix;
  }

  @Getter
  @Setter
  public static class Jwt {
    private String secretKey;
  }

  @Getter
  @Setter
  public static class Flickr {
    private String apikey;
    private String apisecret;
    private String appkey;
    private String appsecret;
  }
}
