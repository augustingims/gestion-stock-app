package com.skysoft.app.infrastructure.config.security;

import com.skysoft.app.infrastructure.config.security.auth.ApplicationUserDetailsService;
import com.skysoft.app.infrastructure.utils.JwtUtil;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Objects;

@Component
@Slf4j
@RequiredArgsConstructor
public class ApplicationRequestFilter extends OncePerRequestFilter {
    private final JwtUtil jwtUtil;
    private final ApplicationUserDetailsService applicationUserDetailsService;
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        final String authHeader = request.getHeader("Authorization");
        String userEmail = null;
        String jwt = null;
        String idEntreprise = null;
        try{
            if(Objects.nonNull(authHeader) && authHeader.startsWith("Bearer ")){
                jwt = authHeader.substring(7);
                userEmail = jwtUtil.extractUsername(jwt);
                idEntreprise = jwtUtil.extractIdEntreprise(jwt);
            }

            if(Objects.nonNull(userEmail) && SecurityContextHolder.getContext().getAuthentication() == null){
                UserDetails userDetails = applicationUserDetailsService.loadUserByUsername(userEmail);
                if (Boolean.TRUE.equals(jwtUtil.isValidToken(jwt, userDetails))) {
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }
            }
            MDC.put("idEntreprise", idEntreprise);
            filterChain.doFilter(request, response);
        } catch (ExpiredJwtException e){
            log.info("Security exception for user {} - {}", e.getClaims().getSubject(), e.getMessage());
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }

    }
}
