package com.skysoft.app.infrastructure.config.security.auth;

import com.skysoft.app.domain.exception.ErrorCodes;
import com.skysoft.app.domain.exception.TechnicalErrorException;
import com.skysoft.app.infrastructure.connector.db.entity.Utilisateur;
import com.skysoft.app.infrastructure.connector.db.repository.UtilisateurJpaRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ApplicationUserDetailsService implements UserDetailsService {

    private final UtilisateurJpaRepository utilisateurJpaRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        Utilisateur user = null;
        try {
            user = utilisateurJpaRepository.findByEmail(username)
                    .orElseThrow(() -> new TechnicalErrorException("Aucun utilisateur avec cette email", ErrorCodes.UTILISATEUR_NOT_FOUND));
        } catch (final TechnicalErrorException e) {
            throw new RuntimeException(e);
        }
        final List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getRoleName())));
        return new ExtendedUser(user.getEmail(), user.getMotDePasse(), user.getEntreprise().getId(), authorities);
    }
}
