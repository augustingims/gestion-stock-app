package com.skysoft.app.infrastructure.config;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.Permission;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.RequestScope;

@Configuration
@RequiredArgsConstructor
public class FlickrConfiguration {
  private final ApplicationProperties applicationProperties;

  @Bean(name = "flickrManager")
  @RequestScope
  public Flickr flickrManager() {
    final Flickr flickr =
        new Flickr(
            applicationProperties.getFlickr().getApikey(),
            applicationProperties.getFlickr().getApisecret(),
            new REST());
    final Auth auth = new Auth();
    auth.setPermission(Permission.READ);
    auth.setToken(applicationProperties.getFlickr().getAppkey());
    auth.setTokenSecret(applicationProperties.getFlickr().getAppsecret());
    final RequestContext requestContext = RequestContext.getRequestContext();
    requestContext.setAuth(auth);
    flickr.setAuth(auth);
    return flickr;
  }

  //  @Bean
  //  public Flickr flickrManager()
  //      throws InterruptedException, ExecutionException, IOException, FlickrException {
  //    final Flickr flickr =
  //        new Flickr(
  //            applicationProperties.getFlickr().getApikey(),
  //            applicationProperties.getFlickr().getApisecret(),
  //            new REST());
  //
  //    final OAuth10aService service =
  //        new ServiceBuilder(applicationProperties.getFlickr().getApikey())
  //            .apiSecret(applicationProperties.getFlickr().getApisecret())
  //            .build(FlickrApi.instance(FlickrApi.FlickrPerm.WRITE));
  //
  //    final Scanner scanner = new Scanner(System.in);
  //
  //    final OAuth1RequestToken request = service.getRequestToken();
  //
  //    final String authUrl = service.getAuthorizationUrl(request);
  //
  //    System.out.println(authUrl);
  //    System.out.println("Paste it here >> ");
  //
  //    final String authVerifier = scanner.nextLine();
  //
  //    final OAuth1AccessToken accessToken = service.getAccessToken(request, authVerifier);
  //
  //    System.out.println(accessToken.getToken());
  //    System.out.println(accessToken.getTokenSecret());
  //
  //    final Auth auth = flickr.getAuthInterface().checkToken(accessToken);
  //
  //    System.out.println("---------------------------");
  //    System.out.println(auth.getToken());
  //    System.out.println(auth.getTokenSecret());
  //
  //    return flickr;
  //  }
}
