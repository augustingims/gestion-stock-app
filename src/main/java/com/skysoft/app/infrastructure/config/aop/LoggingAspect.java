package com.skysoft.app.infrastructure.config.aop;

import java.util.Arrays;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Aspect
@Component
public class LoggingAspect {

  @Pointcut(
      "within(@org.springframework.stereotype.Repository *)"
          + "|| within(@org.springframework.stereotype.Service *)"
          + "|| within(@org.springframework.web.bind.annotation.RestController *)")
  public void springBeanPointcut() {}

  @Pointcut(
      "within(com.skysoft.app.infrastructure.connector.db.adapters..*)"
          + "|| within(com.skysoft.app.application.rest..*)"
          + "|| within(com.skysoft.app.infrastructure.connector.db.repository..*)")
  public void springFolderPointcut() {}

  private Logger logger(final JoinPoint joinPoint) {
    return LoggerFactory.getLogger(joinPoint.getSignature().getDeclaringTypeName());
  }

  @AfterThrowing(pointcut = "springBeanPointcut() && springFolderPointcut()", throwing = "e")
  public void logAfterThrownig(final JoinPoint joinPoint, final Throwable e) {
    logger(joinPoint)
        .error(
            "Exception in {}() with cause = '{}' and exception = '{}'",
            joinPoint.getSignature().getName(),
            e.getCause() != null ? e.getCause() : "NULL",
            e.getMessage(),
            e);
  }

  @Around("springBeanPointcut() && springFolderPointcut()")
  public Object logAround(final ProceedingJoinPoint joinPoint) throws Throwable {
    final Logger log = logger(joinPoint);
    log.debug(
        "Enter: {}() with argument[s] = {}",
        joinPoint.getSignature().getName(),
        joinPoint.getArgs() != null ? Arrays.toString(joinPoint.getArgs()) : "");

    final StopWatch stopWatch = new StopWatch();
    stopWatch.start();
    final Object result = joinPoint.proceed();
    stopWatch.stop();
    log.debug("Exit: {}() with result = {}", joinPoint.getSignature().getName(), result);
    log.info(
        "Execution time of {}.{}() :: {} ms",
        joinPoint.getSignature().getDeclaringType().getSimpleName(),
        joinPoint.getSignature().getName(),
        stopWatch.getTotalTimeMillis());
    return result;
  }
}
