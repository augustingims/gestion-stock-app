package com.skysoft.app.infrastructure.config;

import java.util.concurrent.Executor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
@RequiredArgsConstructor
public class AsyncConfiguration {

  private final ApplicationProperties applicationProperties;

  @Bean
  public Executor taskExecutor() {
    final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(applicationProperties.getTaskAsync().getCorePoolSize());
    executor.setMaxPoolSize(applicationProperties.getTaskAsync().getMaxPoolSize());
    executor.setQueueCapacity(applicationProperties.getTaskAsync().getQueueCapacity());
    executor.setThreadNamePrefix(applicationProperties.getTaskAsync().getThreadNamePrefix());
    executor.initialize();
    return executor;
  }
}
