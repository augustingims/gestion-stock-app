package com.skysoft.app.infrastructure.utils;

import java.util.Optional;
import lombok.NoArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

@NoArgsConstructor
public class SecurityUtils {

    public static Optional<String> getCurrentUserLogin() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(extractPrincipal(securityContext.getAuthentication()));
    }

    private static String extractPrincipal(final Authentication authentication) {
        if(authentication == null){
            return null;
        } else if (authentication.getPrincipal() instanceof final UserDetails springUser){
            return springUser.getUsername();
        } else if (authentication.getPrincipal() instanceof String){
            return String.valueOf(authentication.getPrincipal());
        }
        return null;
    }

}
