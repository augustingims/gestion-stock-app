package com.skysoft.app.infrastructure.utils;

import com.skysoft.app.infrastructure.config.ApplicationProperties;
import com.skysoft.app.infrastructure.config.security.auth.ExtendedUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JwtUtil {
    private final ApplicationProperties applicationProperties;

    private String createToken(final Map<String, Object> claims, final ExtendedUser extendedUser){
    return Jwts.builder()
        .setClaims(claims)
        .setSubject(extendedUser.getUsername())
        .setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60))
        .claim("idEntreprise", extendedUser.getIdEntreprise().toString())
        .signWith(SignatureAlgorithm.HS256, applicationProperties.getJwt().getSecretKey())
        .compact();
    }

    public boolean isValidToken(final String token, final UserDetails userDetails){
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    public String extractUsername(final String token) {
        return extractClaim(token, Claims::getSubject);
    }

    private Date extractExpiration(final String token){
        return extractClaim(token, Claims::getExpiration);
    }

    private boolean isTokenExpired(final String token){
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(final ExtendedUser extendedUser){
        final Map<String, Object> claims = new HashMap<>();
        return createToken(claims, extendedUser);
    }

    private <T> T extractClaim(final String token, final Function<Claims, T> claimsResolve) {
        final Claims claims = extractAllClaims(token);
        return claimsResolve.apply(claims);
    }

    public String extractIdEntreprise(final String token){
        final Claims claims = extractAllClaims(token);
        return claims.get("idEntreprise", String.class);
    }

    private Claims extractAllClaims(final String token) {
        return Jwts.parser().setSigningKey(applicationProperties.getJwt().getSecretKey()).parseClaimsJws(token).getBody();
    }
}
