package com.skysoft.app.infrastructure.connector.db.repository.query;

import com.skysoft.app.infrastructure.connector.db.entity.Article;
import com.skysoft.app.infrastructure.connector.db.repository.query.criteria.ArticleCriteria;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ArticleQueryService {

  public Specification<Article> getSpecification(final ArticleCriteria criteria) {
    log.debug("find by criteria : {}", criteria);
    return createSpecification(criteria);
  }

  private Specification<Article> createSpecification(final ArticleCriteria criteria) {
    Specification<Article> specification = Specification.where(null);

    if (criteria != null) {
      if (criteria.getId() != null) {
        specification = specification.and(buildLongSpecification(criteria.getId(), "id"));
      }
      if (criteria.getCode() != null) {
        specification =
            specification.and(buildStringSpecification(criteria.getCode(), "codeArticle"));
      }
      if (criteria.getCategory() != null) {}
    }
    return specification;
  }

  private Specification<Article> buildLongSpecification(final Long id, final String attributeName) {
    return (root, query, builder) -> builder.equal(root.get(attributeName), id);
  }

  private Specification<Article> buildStringSpecification(
      final String data, final String attributeName) {
    return (root, query, builder) -> builder.equal(root.get(attributeName), data);
  }
}
