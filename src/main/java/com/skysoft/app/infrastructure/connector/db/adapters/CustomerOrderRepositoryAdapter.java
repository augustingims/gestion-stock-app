package com.skysoft.app.infrastructure.connector.db.adapters;

import com.skysoft.app.domain.feature.customer.ports.out.CustomerOrderRepository;
import com.skysoft.app.domain.model.CustomerOrder;
import com.skysoft.app.infrastructure.connector.db.repository.CommandeClientJpaRepository;
import com.skysoft.app.infrastructure.mapper.CommandeClientMapper;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CustomerOrderRepositoryAdapter implements CustomerOrderRepository {
  private final CommandeClientJpaRepository commandeClientJpaRepository;

  @Override
  public CustomerOrder saveCustomerOrder(CustomerOrder customerOrder) {
    return CommandeClientMapper.INSTANCE.toDomain(
        commandeClientJpaRepository.save(CommandeClientMapper.INSTANCE.toEntity(customerOrder)));
  }

  @Override
  public Optional<CustomerOrder> findCustomerOrderById(Long id) {
    return commandeClientJpaRepository.findById(id).map(CommandeClientMapper.INSTANCE::toDomain);
  }

  @Override
  public Optional<CustomerOrder> findCustomerOrderByCode(String code) {
    return commandeClientJpaRepository
        .findByCode(code)
        .map(CommandeClientMapper.INSTANCE::toDomain);
  }

  @Override
  public List<CustomerOrder> findAllCustomerOrder() {
    return commandeClientJpaRepository.findAll().stream()
        .map(CommandeClientMapper.INSTANCE::toDomain)
        .toList();
  }

  @Override
  public List<CustomerOrder> findAllByCustomerId(Long customerId) {
    return commandeClientJpaRepository.findAllByClientId(customerId).stream()
        .map(CommandeClientMapper.INSTANCE::toDomain)
        .toList();
  }

  @Override
  public void deleteCustomerOrder(Long id) {
    commandeClientJpaRepository.deleteById(id);
  }
}
