package com.skysoft.app.infrastructure.connector.flickr;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.skysoft.app.domain.service.out.FlickrService;
import java.io.InputStream;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FlickrServiceAdapter implements FlickrService {
  private final Flickr flickrManager;

  @Override
  @SneakyThrows
  public String savePhoto(final InputStream photo, final String title) throws FlickrException {
    final UploadMetaData uploadMetaData = new UploadMetaData();
    uploadMetaData.setTitle(title);

    final String photoId = flickrManager.getUploader().upload(photo, uploadMetaData);
    return flickrManager.getPhotosInterface().getPhoto(photoId).getMedium640Url();
  }
}
