package com.skysoft.app.infrastructure.connector.db.entity;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(
    name = "article",
    indexes = {
      @Index(name = "idx_code", columnList = "code_article"),
      @Index(name = "idx_categorie_entreprise", columnList = "code_article, id_entreprise"),
      @Index(name = "idx_article_categorie", columnList = "idcategorie")
    })
public class Article extends AbstractEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "code_article")
  private String codeArticle;

  @Column(name = "designation")
  private String designation;

  @Column(name = "prix_unitaire_ht")
  private BigDecimal prixUnitaireHt;

  @Column(name = "prix_unitaire_ttc")
  private BigDecimal prixUnitaireTtc;

  @Column(name = "taux_tva")
  private BigDecimal tauxTva;

  @Column(name = "photo")
  private String photo;

  @Column(name = "id_entreprise")
  private Long idEntreprise;

  @ManyToOne
  @JoinColumn(name = "idcategorie")
  private Categorie categorie;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "article")
  private List<LigneVente> ligneVentes = new ArrayList<>();

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "article")
  private List<LigneCommandeFournisseur> ligneCommandeFournisseurs = new ArrayList<>();

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "article")
  private List<LigneCommandeClient> ligneCommandeClients = new ArrayList<>();

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "article")
  private List<MvtStk> mvtStks = new ArrayList<>();
}
