package com.skysoft.app.infrastructure.connector.db.adapters;

import com.skysoft.app.domain.feature.product.ports.out.ProductRepository;
import com.skysoft.app.domain.model.*;
import com.skysoft.app.infrastructure.connector.db.repository.ArticleJpaRepository;
import com.skysoft.app.infrastructure.connector.db.repository.query.ArticleQueryService;
import com.skysoft.app.infrastructure.connector.db.repository.query.criteria.ArticleCriteria;
import com.skysoft.app.infrastructure.mapper.ArticleMapper;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductRepositoryAdapter implements ProductRepository {
  private final ArticleJpaRepository articleJpaRepository;
  private final ArticleQueryService articleQueryService;

  @Override
  public Product saveArticle(final Product product) {
    return ArticleMapper.INSTANCE.toDomain(
        articleJpaRepository.save(ArticleMapper.INSTANCE.toEntity(product)));
  }

  @Override
  public Optional<Product> findArticleById(final Long productId) {
    return articleJpaRepository.findById(productId).map(ArticleMapper.INSTANCE::toDomain);
  }

  @Override
  public Optional<Product> findByCodeArticle(final String codeProduct) {
    return articleJpaRepository
        .findByCodeArticle(codeProduct)
        .map(ArticleMapper.INSTANCE::toDomain);
  }

  @Override
  public List<Product> findAllArticleByIdCategory(final Long categoryId) {
    return articleJpaRepository.findByCategorieId(categoryId).stream()
        .map(ArticleMapper.INSTANCE::toDomain)
        .toList();
  }

  @Override
  public List<Product> findByCriteria(final Long productId, final String productCode) {
    return articleJpaRepository.findAll(articleQueryService.getSpecification(ArticleCriteria.builder().id(productId).code(productCode).build())).stream().map(ArticleMapper.INSTANCE::toDomain).toList();
  }

  @Override
  public void deleteArticle(final Long productId) {
    articleJpaRepository.deleteById(productId);
  }
}
