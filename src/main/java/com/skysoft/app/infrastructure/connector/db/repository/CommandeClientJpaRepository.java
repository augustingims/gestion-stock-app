package com.skysoft.app.infrastructure.connector.db.repository;

import com.skysoft.app.infrastructure.connector.db.entity.CommandeClient;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommandeClientJpaRepository extends JpaRepository<CommandeClient, Long> {
  List<CommandeClient> findAllByClientId(Long id);

  Optional<CommandeClient> findByCode(String code);
}
