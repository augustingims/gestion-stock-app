package com.skysoft.app.infrastructure.connector.db.entity.enumeration;

public enum EtatCommande {
    LIVREE, VALIDEE, EN_PREPARATION, ANNULEE
}
