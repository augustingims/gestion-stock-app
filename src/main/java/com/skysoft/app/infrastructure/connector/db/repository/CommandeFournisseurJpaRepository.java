package com.skysoft.app.infrastructure.connector.db.repository;

import com.skysoft.app.infrastructure.connector.db.entity.CommandeFournisseur;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommandeFournisseurJpaRepository extends JpaRepository<CommandeFournisseur, Long> {
  List<CommandeFournisseur> findAllByFournisseurId(Long id);

  Optional<CommandeFournisseur> findByCode(String code);
}
