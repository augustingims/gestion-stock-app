package com.skysoft.app.infrastructure.connector.db.repository.query.criteria;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ArticleCriteria {
  private Long id;
  private String code;
  private Long category;
}
