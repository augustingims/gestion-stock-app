package com.skysoft.app.infrastructure.connector.db.adapters;

import com.skysoft.app.domain.feature.product.ports.out.SalesRepository;
import com.skysoft.app.domain.model.Sales;
import com.skysoft.app.infrastructure.connector.db.repository.VenteJpaRepository;
import com.skysoft.app.infrastructure.mapper.VenteMapper;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class SalesRepositoryAdapter implements SalesRepository {
  private final VenteJpaRepository venteJpaRepository;

  @Override
  public Sales saveSales(Sales sales) {
    return VenteMapper.INSTANCE.toDomain(
        venteJpaRepository.save(VenteMapper.INSTANCE.toEntity(sales)));
  }

  @Override
  public Optional<Sales> findSalesById(Long id) {
    return venteJpaRepository.findById(id).map(VenteMapper.INSTANCE::toDomain);
  }

  @Override
  public Optional<Sales> findSalesByCode(String code) {
    return venteJpaRepository.findVenteByCode(code).map(VenteMapper.INSTANCE::toDomain);
  }

  @Override
  public List<Sales> findAllSales() {
    return venteJpaRepository.findAll().stream().map(VenteMapper.INSTANCE::toDomain).toList();
  }

  @Override
  public void deleteSales(Long id) {
    venteJpaRepository.deleteById(id);
  }
}
