package com.skysoft.app.infrastructure.connector.db.entity;

import java.math.BigDecimal;

import jakarta.persistence.*;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "lignecommandefournisseur")
public class LigneCommandeFournisseur extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "quantite")
    private BigDecimal quantite;
    @Column(name = "prix_unitaire")
    private BigDecimal prixUnitaire;
    @Column(name = "id_entreprise")
    private Long idEntreprise;
    @ManyToOne
    @JoinColumn(name = "idcommandefournisseur")
    private CommandeFournisseur commandeFournisseur;
    @ManyToOne
    @JoinColumn(name = "idarticle")
    private Article article;
}
