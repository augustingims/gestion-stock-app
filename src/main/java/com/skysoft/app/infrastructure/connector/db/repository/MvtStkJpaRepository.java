package com.skysoft.app.infrastructure.connector.db.repository;

import com.skysoft.app.infrastructure.connector.db.entity.MvtStk;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MvtStkJpaRepository extends JpaRepository<MvtStk, Long> {
  @Query("select sum(m.quantite) from MvtStk m where m.article.id = :idArtcle")
  BigDecimal stockReelArticle(@Param("idArtcle") Long idArtcle);

  List<MvtStk> findAllByArticleId(Long idArticle);
}
