package com.skysoft.app.infrastructure.connector.db.adapters;

import com.skysoft.app.domain.feature.product.ports.out.LineSalesRepository;
import com.skysoft.app.domain.model.LineSales;
import com.skysoft.app.infrastructure.connector.db.repository.LigneVenteJpaRepository;
import com.skysoft.app.infrastructure.mapper.LigneVenteMapper;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class LineSalesRepositoryAdapter implements LineSalesRepository {
  private final LigneVenteJpaRepository ligneVenteJpaRepository;

  @Override
  public List<LineSales> findHistoriqueVentes(Long productId) {
    return ligneVenteJpaRepository.findAllByArticleId(productId).stream()
        .map(LigneVenteMapper.INSTANCE::toDomain)
        .toList();
  }

  @Override
  public LineSales saveVente(LineSales lineSales) {
    return LigneVenteMapper.INSTANCE.toDomain(
        ligneVenteJpaRepository.save(LigneVenteMapper.INSTANCE.toEntity(lineSales)));
  }

  @Override
  public List<LineSales> findAllByVenteId(Long venteId) {
    return ligneVenteJpaRepository.findAllByVenteId(venteId).stream()
        .map(LigneVenteMapper.INSTANCE::toDomain)
        .toList();
  }
}
