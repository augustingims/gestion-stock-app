package com.skysoft.app.infrastructure.connector.db.repository;

import com.skysoft.app.infrastructure.connector.db.entity.LigneCommandeClient;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LigneCommandeClientJpaRepository extends JpaRepository<LigneCommandeClient, Long> {
  List<LigneCommandeClient> findAllByArticleId(Long idArticle);

  List<LigneCommandeClient> findAllByCommandeClientId(Long id);
}
