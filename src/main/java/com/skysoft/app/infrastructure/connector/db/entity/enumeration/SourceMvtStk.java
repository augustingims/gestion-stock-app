package com.skysoft.app.infrastructure.connector.db.entity.enumeration;

public enum SourceMvtStk {
    COMMANDE_CLIENT, COMMANDE_FOURNISSEUR, VENTE
}
