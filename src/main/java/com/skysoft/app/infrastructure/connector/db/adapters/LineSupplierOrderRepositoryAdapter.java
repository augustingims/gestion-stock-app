package com.skysoft.app.infrastructure.connector.db.adapters;

import com.skysoft.app.domain.feature.supplier.ports.out.LineSupplierOrderRepository;
import com.skysoft.app.domain.model.LineSupplierOrder;
import com.skysoft.app.infrastructure.connector.db.repository.LigneCommandeFournisseurJpaRepository;
import com.skysoft.app.infrastructure.mapper.LigneCommandeFournisseurMapper;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class LineSupplierOrderRepositoryAdapter implements LineSupplierOrderRepository {
  private final LigneCommandeFournisseurJpaRepository ligneCommandeFournisseurJpaRepository;

  @Override
  public List<LineSupplierOrder> findAllLignesCommandesFournisseurByCommandeFournisseurId(
      Long idCommande) {
    return ligneCommandeFournisseurJpaRepository.findAllByCommandeFournisseurId(idCommande).stream()
        .map(LigneCommandeFournisseurMapper.INSTANCE::toDomain)
        .toList();
  }

  @Override
  public List<LineSupplierOrder> findHistoriqueCommandeFournisseur(Long productId) {
    return ligneCommandeFournisseurJpaRepository.findAllByArticleId(productId).stream()
        .map(LigneCommandeFournisseurMapper.INSTANCE::toDomain)
        .toList();
  }

  @Override
  public LineSupplierOrder saveLineSupplierOrder(LineSupplierOrder lineSupplierOrder) {
    return LigneCommandeFournisseurMapper.INSTANCE.toDomain(
        ligneCommandeFournisseurJpaRepository.save(
            LigneCommandeFournisseurMapper.INSTANCE.toEntity(lineSupplierOrder)));
  }

  @Override
  public void deleteLineSupplierOrder(Long lineSupplierOrderId) {
    ligneCommandeFournisseurJpaRepository.deleteById(lineSupplierOrderId);
  }

  @Override
  public Optional<LineSupplierOrder> findByLineSupplierOrderId(Long lineSupplierOrderId) {
    return ligneCommandeFournisseurJpaRepository
        .findById(lineSupplierOrderId)
        .map(LigneCommandeFournisseurMapper.INSTANCE::toDomain);
  }
}
