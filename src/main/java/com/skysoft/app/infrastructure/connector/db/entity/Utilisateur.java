package com.skysoft.app.infrastructure.connector.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import java.time.Instant;
import java.util.List;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "utilisateur")
public class Utilisateur extends AbstractEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "nom")
  private String nom;

  @Column(name = "prenom")
  private String prenom;

  @Column(name = "email")
  private String email;

  @Column(name = "date_naissance")
  private Instant dateNaissance;

  @Column(name = "mot_de_passe")
  private String motDePasse;

  @Embedded private Adresse adresse;

  @Column(name = "photo")
  private String photo;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "utilisateur")
  @JsonIgnore
  private List<Roles> roles;

  @ManyToOne
  @JoinColumn(name = "id_entreprise")
  private Entreprise entreprise;
}
