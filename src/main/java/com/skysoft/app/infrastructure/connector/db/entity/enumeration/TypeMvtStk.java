package com.skysoft.app.infrastructure.connector.db.entity.enumeration;

public enum TypeMvtStk {
    ENTREE, SORTIE, CORRECTION_POS, CORRECTION_NEG
}
