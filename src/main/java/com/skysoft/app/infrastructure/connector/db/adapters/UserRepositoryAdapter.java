package com.skysoft.app.infrastructure.connector.db.adapters;

import com.skysoft.app.domain.feature.user.ports.out.UserRepository;
import com.skysoft.app.domain.model.Authority;
import com.skysoft.app.domain.model.User;
import com.skysoft.app.infrastructure.connector.db.repository.RolesJpaRepository;
import com.skysoft.app.infrastructure.connector.db.repository.UtilisateurJpaRepository;
import com.skysoft.app.infrastructure.connector.db.repository.query.UtilisateurQueryService;
import com.skysoft.app.infrastructure.connector.db.repository.query.criteria.UtilisateurCriteria;
import com.skysoft.app.infrastructure.mapper.RolesMapper;
import com.skysoft.app.infrastructure.mapper.UtilisateurMapper;
import com.skysoft.app.infrastructure.utils.SecurityUtils;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserRepositoryAdapter implements UserRepository {
  private final UtilisateurJpaRepository utilisateurJpaRepository;
  private final RolesJpaRepository rolesJpaRepository;
  private final PasswordEncoder passwordEncoder;
  private final UtilisateurQueryService utilisateurQueryService;

  @Override
  public User saveUser(final User user) {
    user.setMotDePasse(passwordEncoder.encode(user.getMotDePasse()));
    return UtilisateurMapper.INSTANCE.toDomain(
        utilisateurJpaRepository.save(UtilisateurMapper.INSTANCE.toEntity(user)));
  }

  @Override
  public Optional<User> findById(final Long userId) {
    return utilisateurJpaRepository.findById(userId).map(UtilisateurMapper.INSTANCE::toDomain);
  }

  @Override
  public Optional<User> findByEmail(final String email) {
    return utilisateurJpaRepository.findByEmail(email).map(UtilisateurMapper.INSTANCE::toDomain);
  }

  @Override
  public Optional<User> currentUser() {
    return SecurityUtils.getCurrentUserLogin()
            .flatMap(utilisateurJpaRepository::findByEmail)
            .map(UtilisateurMapper.INSTANCE::toDomain);
  }

  @Override
  public List<User> findByCriteria(final Long userId, final String email) {
    return utilisateurJpaRepository.findAll(utilisateurQueryService.getSpecification(UtilisateurCriteria.builder().id(userId).email(email).build())).stream().map(UtilisateurMapper.INSTANCE::toDomain).toList();
  }

  @Override
  public void saveAuthority(final Authority authority) {
    rolesJpaRepository.save(RolesMapper.INSTANCE.toEntity(authority));
  }
}
