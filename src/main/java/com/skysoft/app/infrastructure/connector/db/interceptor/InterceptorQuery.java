package com.skysoft.app.infrastructure.connector.db.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.resource.jdbc.spi.StatementInspector;
import org.slf4j.MDC;
import org.springframework.util.StringUtils;

@Slf4j
public class InterceptorQuery implements StatementInspector {
  @Override
  public String inspect(String sql) {
    if (StringUtils.hasLength(sql) && sql.toLowerCase().startsWith("select")) {
      final String entityName = sql.substring(7, sql.indexOf("."));
      final String idEntreprise = MDC.get("idEntreprise");
      if (StringUtils.hasLength(entityName)
          && StringUtils.hasLength(idEntreprise)
          && !sql.toLowerCase().contains("entreprise")
          && !sql.toLowerCase().contains("roles")) {
        if (sql.contains("where")) {
          sql = sql + " and " + entityName + ".id_entreprise = " + idEntreprise;
        } else {
          sql = sql + " where " + entityName + ".id_entreprise = " + idEntreprise;
        }
      }
    }
    return sql;
  }
}
