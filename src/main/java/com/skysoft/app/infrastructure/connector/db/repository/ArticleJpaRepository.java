package com.skysoft.app.infrastructure.connector.db.repository;

import com.skysoft.app.infrastructure.connector.db.entity.Article;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleJpaRepository
    extends JpaRepository<Article, Long>, JpaSpecificationExecutor<Article> {
  List<Article> findByCategorieId(Long id);

  Optional<Article> findByCodeArticle(String codeArticle);

  Page<Article> findByCategorieId(Long idCategorie, Pageable pageable);
}
