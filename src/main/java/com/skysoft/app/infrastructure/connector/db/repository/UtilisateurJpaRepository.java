package com.skysoft.app.infrastructure.connector.db.repository;

import com.skysoft.app.infrastructure.connector.db.entity.Utilisateur;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilisateurJpaRepository extends JpaRepository<Utilisateur, Long>, JpaSpecificationExecutor<Utilisateur> {
  Optional<Utilisateur> findByEmail(String email);
}
