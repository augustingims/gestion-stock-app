package com.skysoft.app.infrastructure.connector.db.entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
@Embeddable
public class Adresse implements Serializable {

    @Column(name = "adresse1")
    @NotBlank(message = "L'adresse 1 ne doit etre vide.")
    private String adresse1;

    @Column(name = "adresse2")
    private String adresse2;

    @Column(name = "ville")
    @NotBlank(message = "La ville est un champ obligatoire")
    private String ville;

    @Column(name = "code_postale")
    @NotBlank(message = "Ce champ est obligatoire.")
    private String codePostale;

    @Column(name = "pays")
    @NotBlank(message = "Ce champ est obligatoire.")
    private String pays;
}
