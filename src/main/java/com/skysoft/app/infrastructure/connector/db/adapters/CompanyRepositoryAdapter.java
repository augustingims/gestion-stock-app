package com.skysoft.app.infrastructure.connector.db.adapters;

import com.skysoft.app.domain.feature.company.ports.out.CompanyRepository;
import com.skysoft.app.domain.model.Company;
import com.skysoft.app.infrastructure.connector.db.repository.EntrepriseJpaRepository;
import com.skysoft.app.infrastructure.mapper.EntrepriseMapper;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CompanyRepositoryAdapter implements CompanyRepository {

  private final EntrepriseJpaRepository entrepriseJpaRepository;

  @Override
  public Company saveCompany(Company company) {
    return EntrepriseMapper.INSTANCE.toDomain(
        entrepriseJpaRepository.save(EntrepriseMapper.INSTANCE.toEntity(company)));
  }

  @Override
  public Optional<Company> findCompany(Long companyId) {
    return entrepriseJpaRepository.findById(companyId).map(EntrepriseMapper.INSTANCE::toDomain);
  }

  @Override
  public List<Company> findAllCompany() {
    return entrepriseJpaRepository.findAll().stream()
        .map(EntrepriseMapper.INSTANCE::toDomain)
        .toList();
  }
}
