package com.skysoft.app.infrastructure.connector.db.adapters;

import com.skysoft.app.domain.feature.customer.ports.out.CustomerRepository;
import com.skysoft.app.domain.model.Customer;
import com.skysoft.app.infrastructure.connector.db.repository.ClientJpaRepository;
import com.skysoft.app.infrastructure.mapper.ClientMapper;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CustomerRepositoryAdapter implements CustomerRepository {
  private final ClientJpaRepository clientJpaRepository;

  @Override
  public Customer saveCustomer(Customer customer) {
    return ClientMapper.INSTANCE.toDomain(
        clientJpaRepository.save(ClientMapper.INSTANCE.toEntity(customer)));
  }

  @Override
  public Optional<Customer> findCustomerById(Long id) {
    return clientJpaRepository.findById(id).map(ClientMapper.INSTANCE::toDomain);
  }

  @Override
  public List<Customer> findAllCustomer() {
    return clientJpaRepository.findAll().stream().map(ClientMapper.INSTANCE::toDomain).toList();
  }

  @Override
  public void deleteCustomer(Long id) {
    clientJpaRepository.deleteById(id);
  }
}
