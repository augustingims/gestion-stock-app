package com.skysoft.app.infrastructure.connector.db.repository.query;

import com.skysoft.app.infrastructure.connector.db.entity.Utilisateur;
import com.skysoft.app.infrastructure.connector.db.repository.query.criteria.UtilisateurCriteria;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UtilisateurQueryService {

    public Specification<Utilisateur> getSpecification(UtilisateurCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        return  createSpecification(criteria);
    }

    private Specification<Utilisateur> createSpecification(UtilisateurCriteria criteria) {
        Specification<Utilisateur> specification = Specification.where(null);

        if(criteria != null){
            if (criteria.getId() != null) {
                specification = specification.and(buildLongSpecification(criteria.getId(), "id"));
            }
            if (criteria.getNom() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNom(), "nom"));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), "email"));
            }
        }
        return specification;
    }

    private Specification<Utilisateur> buildLongSpecification(Long id, String attributeName) {
        return (root, query, builder) -> builder.equal(root.get(attributeName), id);
    }

    private Specification<Utilisateur> buildStringSpecification(String data, String attributeName) {
        return (root, query, builder) -> builder.equal(root.get(attributeName), data);
    }

}
