package com.skysoft.app.infrastructure.connector.db.adapters;

import com.skysoft.app.domain.feature.supplier.ports.out.SupplierRepository;
import com.skysoft.app.domain.model.Supplier;
import com.skysoft.app.infrastructure.connector.db.repository.FournisseurJpaRepository;
import com.skysoft.app.infrastructure.mapper.FournisseurMapper;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class SupplierRepositoryAdapter implements SupplierRepository {
  private final FournisseurJpaRepository fournisseurJpaRepository;

  @Override
  public Supplier saveSupplier(Supplier supplier) {
    return FournisseurMapper.INSTANCE.toDomain(
        fournisseurJpaRepository.save(FournisseurMapper.INSTANCE.toEntity(supplier)));
  }

  @Override
  public Optional<Supplier> findSupplierById(Long supplierId) {
    return fournisseurJpaRepository.findById(supplierId).map(FournisseurMapper.INSTANCE::toDomain);
  }

  @Override
  public List<Supplier> findAllSupplier() {
    return fournisseurJpaRepository.findAll().stream()
        .map(FournisseurMapper.INSTANCE::toDomain)
        .toList();
  }

  @Override
  public void deleteSupplier(Long supplierId) {
    fournisseurJpaRepository.deleteById(supplierId);
  }
}
