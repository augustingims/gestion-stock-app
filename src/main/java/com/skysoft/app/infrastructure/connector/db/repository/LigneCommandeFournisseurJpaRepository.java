package com.skysoft.app.infrastructure.connector.db.repository;

import com.skysoft.app.infrastructure.connector.db.entity.LigneCommandeFournisseur;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LigneCommandeFournisseurJpaRepository
    extends JpaRepository<LigneCommandeFournisseur, Long> {
  List<LigneCommandeFournisseur> findAllByArticleId(Long idArticle);

  List<LigneCommandeFournisseur> findAllByCommandeFournisseurId(Long id);
}
