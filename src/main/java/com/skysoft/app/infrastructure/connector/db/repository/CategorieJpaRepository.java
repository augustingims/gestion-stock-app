package com.skysoft.app.infrastructure.connector.db.repository;

import com.skysoft.app.infrastructure.connector.db.entity.Categorie;
import java.util.Optional;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategorieJpaRepository extends JpaRepository<Categorie, Long> {
  @Cacheable(cacheNames = "findByCode", key = "#code")
  Optional<Categorie> findByCode(String code);
}
