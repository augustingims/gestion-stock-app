package com.skysoft.app.infrastructure.connector.db.adapters;

import com.skysoft.app.domain.feature.product.ports.out.InventoryMovementRepository;
import com.skysoft.app.domain.model.InventoryMovement;
import com.skysoft.app.infrastructure.connector.db.repository.MvtStkJpaRepository;
import com.skysoft.app.infrastructure.mapper.MvtStkMapper;
import java.math.BigDecimal;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class InventoryMovementRepositoryAdapter implements InventoryMovementRepository {
  private final MvtStkJpaRepository mvtStkJpaRepository;

  @Override
  public BigDecimal stockReelArticle(Long productId) {
    return mvtStkJpaRepository.stockReelArticle(productId);
  }

  @Override
  public List<InventoryMovement> mvtStkArticle(Long productId) {
    return mvtStkJpaRepository.findAllByArticleId(productId).stream()
        .map(MvtStkMapper.INSTANCE::toDomain)
        .toList();
  }

  @Override
  public InventoryMovement saveInventoryMovement(InventoryMovement inventoryMovement) {
    return MvtStkMapper.INSTANCE.toDomain(
        mvtStkJpaRepository.save(MvtStkMapper.INSTANCE.toEntity(inventoryMovement)));
  }
}
