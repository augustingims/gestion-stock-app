package com.skysoft.app.infrastructure.connector.db.adapters;

import com.skysoft.app.domain.feature.customer.ports.out.LineCustomerOrderRepository;
import com.skysoft.app.domain.model.LineCustomerOrder;
import com.skysoft.app.infrastructure.connector.db.repository.LigneCommandeClientJpaRepository;
import com.skysoft.app.infrastructure.mapper.LigneCommandeClientMapper;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class LineCustomerOrderRepositoryAdapter implements LineCustomerOrderRepository {
  private final LigneCommandeClientJpaRepository ligneCommandeClientJpaRepository;

  @Override
  public List<LineCustomerOrder> findAllLignesCommandesClientByCommandeClientId(Long idCommande) {
    return ligneCommandeClientJpaRepository.findAllByCommandeClientId(idCommande).stream()
        .map(LigneCommandeClientMapper.INSTANCE::toDomain)
        .toList();
  }

  @Override
  public List<LineCustomerOrder> findHistoriaueCommandeClient(Long productId) {
    return ligneCommandeClientJpaRepository.findAllByArticleId(productId).stream()
        .map(LigneCommandeClientMapper.INSTANCE::toDomain)
        .toList();
  }

  @Override
  public LineCustomerOrder saveLineCustomerOrder(LineCustomerOrder lineCustomerOrder) {
    return LigneCommandeClientMapper.INSTANCE.toDomain(
        ligneCommandeClientJpaRepository.save(
            LigneCommandeClientMapper.INSTANCE.toEntity(lineCustomerOrder)));
  }

  @Override
  public void deleteLineCustomerOrder(Long lineCustomerOrderId) {
    ligneCommandeClientJpaRepository.deleteById(lineCustomerOrderId);
  }

  @Override
  public Optional<LineCustomerOrder> findByLineCustomerOrderId(Long idLigneCommande) {
    return ligneCommandeClientJpaRepository
        .findById(idLigneCommande)
        .map(LigneCommandeClientMapper.INSTANCE::toDomain);
  }
}
