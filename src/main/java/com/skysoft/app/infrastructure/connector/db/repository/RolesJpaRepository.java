package com.skysoft.app.infrastructure.connector.db.repository;

import com.skysoft.app.infrastructure.connector.db.entity.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolesJpaRepository extends JpaRepository<Roles, Long> {}
