package com.skysoft.app.infrastructure.connector.db.entity;

import java.math.BigDecimal;

import jakarta.persistence.*;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "lignecommandeclient")
public class LigneCommandeClient extends AbstractEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "quantite")
  private BigDecimal quantite;

  @Column(name = "prix_unitaire")
  private BigDecimal prixUnitaire;

  @Column(name = "id_entreprise")
  private Long idEntreprise;

  @ManyToOne
  @JoinColumn(name = "idcommandeclient")
  private CommandeClient commandeClient;

  @ManyToOne
  @JoinColumn(name = "idarticle")
  private Article article;
}
