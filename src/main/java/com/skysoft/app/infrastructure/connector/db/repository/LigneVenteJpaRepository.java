package com.skysoft.app.infrastructure.connector.db.repository;

import com.skysoft.app.infrastructure.connector.db.entity.LigneVente;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LigneVenteJpaRepository extends JpaRepository<LigneVente, Long> {
  List<LigneVente> findAllByArticleId(Long idArticle);

  List<LigneVente> findAllByVenteId(Long id);
}
