package com.skysoft.app.infrastructure.connector.db.repository.query.criteria;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UtilisateurCriteria {
    private Long id;
    private String nom;
    private String email;
}
