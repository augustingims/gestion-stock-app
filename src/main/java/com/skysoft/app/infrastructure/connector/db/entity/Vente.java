package com.skysoft.app.infrastructure.connector.db.entity;

import java.time.Instant;
import java.util.List;

import jakarta.persistence.*;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "vente")
public class Vente extends AbstractEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "code")
    private String code;
    @Column(name = "date_vente")
    private Instant dateVente;
    @Column(name = "commentaire")
    private String commentaire;
    @Column(name = "id_entreprise")
    private Long idEntreprise;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "vente")
    private List<LigneVente> ligneVentes;
}
