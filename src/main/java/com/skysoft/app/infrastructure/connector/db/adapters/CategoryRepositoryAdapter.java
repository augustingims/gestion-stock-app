package com.skysoft.app.infrastructure.connector.db.adapters;

import com.skysoft.app.domain.feature.product.ports.out.CategoryRepository;
import com.skysoft.app.domain.model.Category;
import com.skysoft.app.infrastructure.connector.db.repository.CategorieJpaRepository;
import com.skysoft.app.infrastructure.mapper.CategorieMapper;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CategoryRepositoryAdapter implements CategoryRepository {
  private final CategorieJpaRepository categorieJpaRepository;

  @Override
  public Category saveCategory(final Category dto) {
    return CategorieMapper.INSTANCE.toDomain(
        categorieJpaRepository.save(CategorieMapper.INSTANCE.toEntity(dto)));
  }

  @Override
  // @Cacheable(cacheNames = "findCategoryById", key = "#categoryId")
  public Optional<Category> findCategoryById(final Long categoryId) {
    return categorieJpaRepository.findById(categoryId).map(CategorieMapper.INSTANCE::toDomain);
  }

  @Override
  public Optional<Category> findCategoryByCode(final String code) {
    return categorieJpaRepository.findByCode(code).map(CategorieMapper.INSTANCE::toDomain);
  }

  @Override
  public List<Category> findAllCategory() {
    return categorieJpaRepository.findAll().stream()
        .map(CategorieMapper.INSTANCE::toDomain)
        .toList();
  }

  @Override
  public void deleteCategory(final Long categoryId) {
    categorieJpaRepository.deleteById(categoryId);
  }
}
