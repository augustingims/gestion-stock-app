package com.skysoft.app.infrastructure.connector.db.adapters;

import com.skysoft.app.domain.feature.supplier.ports.out.SupplierOrderRepository;
import com.skysoft.app.domain.model.SupplierOrder;
import com.skysoft.app.infrastructure.connector.db.repository.CommandeFournisseurJpaRepository;
import com.skysoft.app.infrastructure.mapper.CommandeFournisseurMapper;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class SupplierOrderRepositoryAdapter implements SupplierOrderRepository {
  private final CommandeFournisseurJpaRepository commandeFournisseurJpaRepository;

  @Override
  public SupplierOrder saveSupplierOrder(SupplierOrder supplierOrder) {
    return CommandeFournisseurMapper.INSTANCE.toDomain(
        commandeFournisseurJpaRepository.save(
            CommandeFournisseurMapper.INSTANCE.toEntity(supplierOrder)));
  }

  @Override
  public Optional<SupplierOrder> findSupplierOrderById(Long id) {
    return commandeFournisseurJpaRepository
        .findById(id)
        .map(CommandeFournisseurMapper.INSTANCE::toDomain);
  }

  @Override
  public List<SupplierOrder> findAllSupplierOrderBySupplierId(Long supplierId) {
    return commandeFournisseurJpaRepository.findAllByFournisseurId(supplierId).stream()
        .map(CommandeFournisseurMapper.INSTANCE::toDomain)
        .toList();
  }

  @Override
  public Optional<SupplierOrder> findSupplierOrderByCode(String code) {
    return commandeFournisseurJpaRepository
        .findByCode(code)
        .map(CommandeFournisseurMapper.INSTANCE::toDomain);
  }

  @Override
  public List<SupplierOrder> findAllSupplierOrder() {
    return commandeFournisseurJpaRepository.findAll().stream()
        .map(CommandeFournisseurMapper.INSTANCE::toDomain)
        .toList();
  }

  @Override
  public void deleteSupplierOrder(Long id) {
    commandeFournisseurJpaRepository.deleteById(id);
  }
}
