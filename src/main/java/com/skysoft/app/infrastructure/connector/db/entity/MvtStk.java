package com.skysoft.app.infrastructure.connector.db.entity;

import java.math.BigDecimal;
import java.time.Instant;

import com.skysoft.app.infrastructure.connector.db.entity.enumeration.SourceMvtStk;
import com.skysoft.app.infrastructure.connector.db.entity.enumeration.TypeMvtStk;
import jakarta.persistence.*;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "mvtstk")
public class MvtStk extends AbstractEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_mvt")
    private Instant dateMvt;

    @Column(name = "quantite")
    private BigDecimal quantite;

    @Column(name = "type_mvt")
    @Enumerated(EnumType.STRING)
    private TypeMvtStk typeMvtStk;

    @Column(name = "source_mvt")
    @Enumerated(EnumType.STRING)
    private SourceMvtStk sourceMvtStk;

    @Column(name = "id_entreprise")
    private Long idEntreprise;

    @ManyToOne
    @JoinColumn(name = "idarticle")
    private Article article;
}
